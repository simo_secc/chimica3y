function F=fEs1PdIC(X)

global a b c d nin2 HinNH3

syms T 

 Hinaria=sum(nin2.*int(a+b.*T+c.*T^2+d.*T^3,175,X));
 Hinaria=eval(Hinaria);
 
 F=7000-HinNH3-Hinaria;