function F=f2Es1PdIC(X)

% Definiamo le seguenti incognite:
% X(1)= nNO22
% X(2)= nNOric
% X(3)= lambda1
% X(4)= lambda2
% X(5)= nO2agg

% Il vettore delle specie �: S=[Aria NO2 NO HNO3 H20 O2 N2 NH3 N2O4]'

global nu1 nu2

n1=[0 0 8.24+X(2) 0 12.744 5.567+X(5) 60.54+(0.79/0.21)*X(5) 0 0]';
n2=n1+nu1*X(3)+nu2*X(4);
ntot=sum(n2);

% Scriviamo ora il sistema di equazioni da risolvere:

F(1)=X(1)-2*X(3)+2*X(4);
F(2)=8.24+X(2)-2*X(3);
F(3)=5.567+X(5)-X(3);
F(4)=ntot/(780e3/101325)*(X(4)/(X(1))^2)-0.698e-9*exp(6860/(180+273.15));
F(5)=X(2)-(1/3)*X(1)-(2/3)*X(4);