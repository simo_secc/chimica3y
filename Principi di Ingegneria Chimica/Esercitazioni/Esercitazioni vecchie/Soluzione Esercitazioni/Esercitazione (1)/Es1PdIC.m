% Risoluzione dell'esercitazione (1) di Principi di Ingegneria Chimica:
% Processo di produzione di acido nitrico: ossidazione di ammoniaca e
% monossido di azoto

clc
clear all
close all


%% Dati:
 % Si consideri il seguente vettore delle specie: S=[Aria NO2 NO HNO3 H20
 % O2 N2 NH3 N2O4]
 
 global a b c d nin2 HinNH3 nu1 nu2
 
 DeltaHf=[0 33.8 90.37 -206.57 -241.83 0 0 -46.19 +9.3]'*1e3;

 % cp= a+bT+cT^2+dT^3, con T in [�C]
 
 a=[28.94 36.07 29.50 110.0 33.46 29.10 29.00 35.15 75.7]';
 b=[0.4147 3.97 0.8188 0 0.6880 1.158 0.2199 2.954 12.5]'*1e-2;
 c=[0.3191 -2.88 -0.2925 0 0.7640 -0.6076 0.5723 0.4421 -11.3]'*1e-5;
 d=[-1.965 7.87 0.3652 0 -3.593 1.311 -2.871 -6.686 0]'*1e-9;
 
 %% Punto 1) : Risolto sul foglio
 
 %% Punto 2) : Verifica del bilancio energetico
 
 nin1=[0 0 0 0 0 0 0 8.497 0]';
 
 syms T
 
 HinNH3=sum(nin1.*int(a+b.*T+c.*T^2+d.*T^3,175,90));
 HinNH3=eval(HinNH3);
 
 nin2=[76.48 0 0 0 0 0 0 0 0]';
 
 X0=180;
 
 T=fsolve('fEs1PdIC',X0);
 
 disp(['Il valore assunto dalla temperatura in ingresso di aria � pari a ',num2str(T)])
 
 pause
 
 %% Punto 3)
 
 nu=[0 0 3.88 0 6 -4.94 0.06 -4 0]';
 lambda=8.497/4;
 nin=[0 0 0 0 0 16.06 60.42 8.497 0]';
 nout=nin+nu*lambda;
 
 syms T
 
 Hin=sum(nin.*(DeltaHf+int(a+b.*T+c.*T^2+d.*T^3,25,175)));
 Hin=eval(Hin);
 Hout=sum(nout.*(DeltaHf+int(a+b.*T+c.*T^2+d.*T^3,25,700)));
 Hout=eval(Hout);
 Q=Hout-Hin;
 disp(['Il valore assunto dalla potenza termica scambiata dal reattore � pari a ',num2str(Q)])
 
 pause
 
 %% Punti 4) e 5)
 
 nu1=[0 2 -2 0 0 -1 0 0 0]';
 nu2=[0 -2 0 0 0 0 0 0 1]';
 
 X0=[10 10 10 10 10]';
 
 X=fsolve('f2Es1PdIC',X0);
 
 disp(['Il valore assunto dalla portata di NO2 � pari a ',num2str((X(1))*3.6),' [kmol/h]'])
 disp(['Il valore assunto dalla portata di NO riciclata � pari a ',num2str((X(2))*3.6),' [kmol/h]'])
 disp(['Il valore assunto da lambda1 � pari a ',num2str((X(3))*3.6),' [kmol/h]'])
 disp(['Il valore assunto da lambda2 � pari a ',num2str((X(4))*3.6),' [kmol/h]'])
 disp(['Il valore assunto dalla portata di O2 aggiunta � pari a ',num2str((X(5))*3.6),' [kmol/h]'])
 

n1=[0 0 8.24+X(2) 0 12.744 5.567+X(5) 60.54+(0.79/0.21)*X(5) 0 0]';
n2=n1+nu1*X(3)+nu2*X(4);
ntot=sum(n2);

x=n2./ntot;

disp(['Il valore assunto dalla portata di H20 � pari a ',num2str((n2(5))*3.6),' [kmol/h]'])
disp(['Il valore assunto dalla portata di N2O4 � pari a ',num2str((n2(9))*3.6),' [kmol/h]'])
disp(['Il valore assunto dalla frazione molare di NO2 � pari a ',num2str(x(2))])
disp(['Il valore assunto dalla frazione molare di H2O � pari a ',num2str(x(5))])
disp(['Il valore assunto dalla frazione molare di N2 � pari a ',num2str(x(7))])
disp(['Il valore assunto dalla frazione molare di N2O4 � pari a ',num2str(x(9))])

