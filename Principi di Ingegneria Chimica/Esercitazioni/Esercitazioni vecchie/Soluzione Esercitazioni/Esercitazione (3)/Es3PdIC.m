clc
clear all
close all

global taug rog mug Dint 
taug=3.37;
rog=1.2;
mug=1.81e-5;
Dint=2e-2-2*6.87e-4;
D=2e-2;
ml=1.667e-2;
mg=4.95695e-3;
rol=1000;
mul=1e-3;
g=9.81;

% Esercizio 3), condizione di loading

disp('---------- Caso a) : condizione di loading ----------')

X0=1;

X=fsolve('fEs3PdIC',X0);

disp(['Il valore assunto dalla velocit� � pari a ',num2str(X),' [m/s]'])

V=X*rog*pi*Dint^2/4;

disp(['Il valore assunto dalla portata di gas � pari a ',num2str(V),' [kg/s]'])

pause

% Esercizio 3), portata pari alla met� di quella di loading

disp('---------- Caso b) : portata pari alla met� di quella di loading ----------')

s(1)=(3*(ml/(pi*D))*mul/(rol^2*g))^(1/3);
vg(1)=4*mg/(pi*(D-2*s(1))^2*rog);
Reg(1)=rog*vg(1)*(D-2*s(1))/mug;
f(1)=16/Reg(1)+0.079*(Reg(1))^(-0.25);
tau(1)=(f(1)*rog*(vg(1))^2)/2;
s(2)=((ml/(pi*(D-s(1)))+rol*tau(1)*(s(1))^2/(2*mul))*(3*mul/((rol)^2*g)))^(1/3);
vg(2)=4*mg/(pi*(D-2*s(2))^2*rog);
Reg(2)=rog*vg(2)*(D-2*s(2))/mug;
f(2)=16/Reg(2)+0.079*(Reg(2))^(-0.25);
tau(2)=(f(2)*rog*(vg(2))^2)/2;
i=2;
while abs(s(i)-s(i-1))>1e-30
s(i+1)=((ml/(pi*(D-s(i)))+rol*tau(i)*(s(i))^2/(2*mul))*(3*mul/((rol)^2*g)))^(1/3);
vg(i+1)=4*mg/(pi*(D-2*s(i+1))^2*rog);
Reg(i+1)=rog*vg(i+1)*(D-2*s(i+1))/mug;
f(i+1)=16/Reg(i+1)+0.079*(Reg(i+1))^(-0.25);
tau(i+1)=(f(i+1)*rog*(vg(i+1))^2)/2;
i=i+1;
end

disp(['Il valore assunto dallo spessore � pari a ',num2str(s(i)),' [m]'])
disp(['Il valore assunto dallo sforzo taug � pari a ',num2str(tau(i)),' [Pa]'])
disp(['Il valore assunto dalla velocit� del gas � pari a ',num2str(vg(i)),' [m/s]'])