% Risoluzione dell'esercitazione n.5 di P.d.I.C

clc
clear all
close all

%% Esercizio n.4

disp('---------- Esercizio (4) ----------')

% Dati:

Di=1e-4;
v=10;
yO2=0.1;
Df=1e-6;
rog=800;
Diffg=2e-5;
nug=5e-5;
T=973.15;
P=101325;
R=8.314;
PM=(6*12.011+14*1.00794)*1e-3;

% Risoluzione dell'equazione differenziale, basata sul metodo di Eulero
deltaD=(Df-Di)/10000;
Sc=nug/Diffg;
D(1)=Di;
t(1)=0;
i=1;
while D(i)>Df
    Re(i)=v*D(i)/nug;
    Sh(i)=2+(1.6*Re(i)^(1/3)+0.6*Re(i)^0.5+5e-3*Re(i)^0.8)*Sc^(1/3);
    Kc(i)=Sh(i)*Diffg/D(i);
    i=i+1;
    t(i)=t(i-1)-(19*rog*R*T*deltaD)/(4*P*yO2*PM*Kc(i-1));
    D(i)=D(i-1)+deltaD;
end

disp(['Il tempo necessario alla combustione della particella � pari a ',num2str(t(i))])

pause

%% Esercizio n.5

disp('---------- Esercizio (5) ----------')

% Dati:
Di=1e-1;
deltaT=15;
v=0.5;
Df=0;
nua=1e-6;
ka=0.5986;
deltahf=6012.065;
rog=920;
roa=1000;
cpa=4185.8;

% Risoluzione dell'equazione differenziale, basata sul metodo di Eulero

deltaD=(Df-Di)/10000;
alfaa=ka/(roa*cpa);
Pr=nua/alfaa;
D(1)=Di;
t(1)=0;
i=1;
while D(i)>Df
    Re(i)=v*D(i)/nua;
    Nu(i)=2+(1.6*Re(i)^(1/3)+0.6*Re(i)^0.5+5e-3*Re(i)^0.8)*Pr^(1/3);
    h(i)=Nu(i)*ka/D(i);
    i=i+1;
    t(i)=t(i-1)-(rog*D(i-1)*deltahf*deltaD)/(2*Nu(i-1)*ka*deltaT);
    D(i)=D(i-1)+deltaD;
end

disp(['Il tempo necessario allo scioglimento della sfera di ghiaccio � pari a ',num2str(t(i))])
   