function F=fEs5Es6PdIC(X)

global hi Ta Tb D De RcoibL

F(1)=hi*pi*D*(X(1)-Tb)-(X(2)-X(1))/RcoibL;
F(2)=hi*pi*D*(X(1)-Tb)-pi*(De^0.75)*1.30225*(Ta-X(2))^(5/4);