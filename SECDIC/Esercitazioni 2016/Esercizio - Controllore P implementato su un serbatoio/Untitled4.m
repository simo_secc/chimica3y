clear all
clc

global A cd Fin Ad g hsp Kc span

A=5; %m2
cd=0.61;
d=0.18;%m
Fin=0.1; %m3/s
Ad=pi/4*d^2;
g=9.80665;%m/s2

Kc=-1;
ho=4;
hss=(Fin/(Ad*cd))^2/(2*g);
hsp=2*hss;
span=500;
[t,h]=ode23s('ODEP',[0 7*span],ho);
plot(t,h)
grid on