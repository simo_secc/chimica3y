function f=ODEchiusoPI(t,y)

global Fino A1 A2 R1 Kc tauI h2sp span

h1=y(1);
h2=y(2);

F1=(h1)/R1;

if t<2*span
    F2=1.43*h2;
    Fin=Fino;
    f(3)=0;
else
    F2=1.9*h2;
    epsi=h2sp-h2;
    integrale=y(3);    
    Fin=Fino+Kc*epsi+Kc/tauI*integrale; 
    f(3)=epsi;
end    
    
if Fin<0
    Fin=0;
end

f(1)=(Fin-F1)/A1;
f(2)=(F1-F2)/A2;
f=f';