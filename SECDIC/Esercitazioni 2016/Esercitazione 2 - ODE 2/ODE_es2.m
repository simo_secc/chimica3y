function f=ODE_es2(t,y)

global m cp Tino Fin Q

T=y(1);

if t<200
    Tin=Tino;
elseif t>=200
    Tin=Tino+30;
end

f=(Fin*cp*(Tin-T)+Q)/(m*cp);


end

