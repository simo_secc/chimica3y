clear all
clc

global F1o F2 c1 c2 V couto

F1o=2; %m3/s
F2=10;
c1=0.5; %kmol/s
c2=6;
V=0.5; %m3

couto=(F1o*c1+F2*c2)/(F1o+F2);
[t,cout]=ode23s('ODE_es3',[0 500],couto);
plot(t,cout)
grid on
