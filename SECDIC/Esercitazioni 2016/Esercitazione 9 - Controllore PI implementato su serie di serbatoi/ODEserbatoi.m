function f=ODEserbatoi(t,y)

global A1 Ad1 A2 Ad2 cd Fin g h1 h2

h1=y(1);
h2=y(2);

F1=cd*Ad1*sqrt(2*g*h1);
F2=cd*Ad2*sqrt(2*g*h2);
f(1)=(Fin-F1)/A1;
f(2)=(F1-F2)/A2;
f=f';

end