function f=ODEaperto(t,y)

global Fino A1 A2 R1 span

h1=y(1);
h2=y(2);

if t<2*span
    Fin=Fino;
else
    Fin=2*Fino;
end    
     
F1=(h1)/R1;
F2=1.43*h2;

f(1)=(Fin-F1)/A1;
f(2)=(F1-F2)/A2;
f=f';