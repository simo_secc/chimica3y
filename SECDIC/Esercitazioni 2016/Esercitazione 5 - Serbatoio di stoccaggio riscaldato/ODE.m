function f=ODE(t,y)

global D Tin Te ro cp Fin Fout U Qi A T i
h=y(1);
T=y(2);

Abase=pi*D^2/4;
Alat=pi*D*h;
A=Abase+Alat;
Qi(1)=U*A*(Te-T(1));

if t>=(10*60) && t<(20*60)
    Fin=10/60; %m3/s
    Fout=0; %m3/s
elseif t>=(30*60) && t<(48*60)
    Fin=5/60; %m3/s
    Fout=10/60; %m3/s
else
    Fin=10/60; %m3/s
    Fout=10/60; %m3/s    
end

f(1,:)=(Fin-Fout)/Abase;
f(2,:)=(ro*Fin*cp*(Tin-T)+U*A*(Te-T)-Qi(i))/(ro*cp*Abase*h);

end

 



