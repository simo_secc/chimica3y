function status=fun(t,y,flag)

global Qi U A Te T i

if strcmp(flag,'init')  %inizializzazione integrazione
    %Qi(1)=U*A*(Te-T);
elseif strcmp(flag,'done')  %termine integrazione (in questo caso non si fa niente)
    
else
    i=i+1;
    Qi(i)=U*A*(Te-T); 
end

status=0;   %se status=1 l'integrazione si arresta

end