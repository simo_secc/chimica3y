function main
    clear all
    close all
    clc
    global lambda
    
    lambda=1;
    
    %metodo Eulero esplicito
    i=1;
    tEu(i)=0;
    yEu(i)=5;
    h=0.01; %passo, arbitrario
    while tEu(i)<20
        i=i+1;
        tEu(i)=tEu(i-1)+h;
        yEu(i)=yEu(i-1)-h*lambda*yEu(i-1);
    end
        
    %metodo Runge-Kutta 4,5 ordine
    [tRK,yRK]=ode45(@fdiff,[0 20],5);
        
    %soluzione analitica
    tan=[0:0.01:20];
    yan=5*exp(-lambda*tan);
    
    plot(tEu,yEu,'r',tRK,yRK,'b',tan,yan,'g')
    grid on
end

function f=fdiff(t,y)
global lambda

f=-lambda*y;
end
