clear all
clc

global A csi ho

A=3; %m2
ho=7; %m
g=9.80665; %m/s2
csi=0.8*sqrt(2*g);

options=odeset('AbsTol', 1e-8, 'RelTol', 1e-10);
[t,h]=ode45('ODE',[0 10],ho,options);
plot(t,h,'r');
grid on
hold on

[tlin,hlin]=ode45('ODElin',[0 10],ho,options);
plot(tlin,hlin,'b');