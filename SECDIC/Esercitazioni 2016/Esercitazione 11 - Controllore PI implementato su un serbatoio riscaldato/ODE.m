function f=ODE(t,y)

global ro cp V Fin Tin DHev span Kc tauI mvo

T=y(1);
integrale=y(2);

if t<span
    epsi=0;
    mv=mvo;
else
    Tsp=90;    
    epsi=Tsp-T;
    mv=mvo+Kc*epsi+Kc/tauI*integrale;
end

f(1)=(ro*Fin*cp*(Tin-T)+mv*DHev)/(ro*V*cp);
f(2)=epsi;
f=f';
