clear all
clc
close all

global ro cp V Fin Tin Tss DHev span Kc tauI mvo

ro=1000;%kg/m3
cp=4186;%J/kgK
tau=2*60;%s
V=5;%m3
Fin=V/tau;
Tin=30;%�C
Tss=70;
DHev=40650/0.018;%J/kg

%stazionario
mvo=-ro*Fin*cp*(Tin-Tss)/DHev

%cambiamento sp
Kc=0.001;
tauI=30;%s
span=500;
options=odeset('RelTol',1e-8,'AbsTol',1e-12);
[t,y]=ode45('ODE',[0 30*span],[Tss 0],options);
T=y(:,1);
plot(t/60,T)
grid on
xlabel('t [min]')
ylabel('T [�C]')