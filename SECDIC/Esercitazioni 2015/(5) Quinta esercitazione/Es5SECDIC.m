%% Risoluzione dell'esercitazione (5) di SECDIC

% La velocit� della portata effluente � direttamente proporzionale alla
% radice di 2 per g per h, questa formula deriva dal teorema di Bernoulli.
% La costante di proporzionalit� corrisponde a Cd, ossia la costante di
% efflusso che tiene conto delle irregolarit� del moto (Bernulli infatti �
% un'idealizzazione). Cd � direttamente correlata alla geometria del punto
% in cui avviene l'efflusso



clc
clear all
close all

global Fin Cd Aforo g Abase

% Dati:
D=12; % [m]
H=7; % [m]
d=0.50; % [m]
Cd=0.61;
Fin=1; % [m^3/s]
h0=4.72; % [m]
g=9.81; % [m/s^2]
Aforo=pi*d^2/4; % [m^2]
Abase=pi*D^2/4; % [m^2]

% Risoluzione:

% Punto 1):
% Parte algebrica: Fin=Fout; Fin=Aforo*Cd*sqrt(2*g*h)

hstaz=((Fin/(Cd*Aforo))^2)/(2*g);

disp(['La altezza del serbatoio nel caso stazionario � pari a ',num2str(hstaz),' [m]'])

% Consideriamo il bilancio materiale sul serbatoio.
% dmdt=roFin-roFout; dVdt=Fin-Fout; dhdt=(Fin-Fout)/A;

options=odeset('RelTol',1E-8,'AbsTol',1E-12); 
tSpan=0:0.1:10000; % [s]
[t,h]=ode45('fEx1Es5',tSpan,h0,options);


figure('Name','Andamento di h','NumberTitle','off')
plot(t,h)
grid on
legend('h')
xlabel('Tempo [s]')
ylabel('Livello del serbatoio [m]')
title('Andamento della h con il tempo')

% Punto 2)

hstaz=((Fin*0.67/(Cd*Aforo))^2)/(2*g);

options=odeset('RelTol',1E-8,'AbsTol',1E-12); 
tSpan=0:0.1:10000; % [s]
[t,h]=ode45('fEx1Es5',tSpan,hstaz,options);


figure('Name','Andamento di h (2)','NumberTitle','off')
plot(t,h)
grid on
legend('h')
xlabel('Tempo [s]')
ylabel('Livello del serbatoio [m]')
title('Andamento della h con il tempo')

% Punto 3)

hstaz=((Fin*0.5/(Cd*Aforo))^2)/(2*g);

options=odeset('RelTol',1E-8,'AbsTol',1E-12); 
tSpan=0:0.1:10000; % [s]
[t,h]=ode45('fEx1Es5',tSpan,hstaz,options);


figure('Name','Andamento di h (3)','NumberTitle','off')
plot(t,h)
grid on
legend('h')
xlabel('Tempo [s]')
ylabel('Livello del serbatoio [m]')
title('Andamento della h con il tempo')

% Determinazione di quanto tempo serve per raggiungere lo stato stazionario
% Sfruttiamo la definizione di rapporto incrementale: possiamo riconoscere
% di essere in una zona di stazionariet� quando il valore assoluto del
% rapporto incrementale � molto basso (in questo caso abbiamo scelto come
% valore 1e-6)
i=1;
while abs((h(i)-h(i+1))/(t(i)-t(i+1)))>1e-6
    i=i+1;
end
disp(['Il tempo necessario per raggiungere lo stato stazionario � pari a ',num2str(t(i)),' [s]'])