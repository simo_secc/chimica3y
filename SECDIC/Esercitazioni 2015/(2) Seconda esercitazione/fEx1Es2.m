function dcdt=fEx1Es2(t,c)
%c(1)=B c(2)=S 
global k1 k2 k3
dcdt=[k1*c(1)*c(2)/(k2+c(2)); -k3*(k1*c(1)*c(2)/(k2+c(2)))];