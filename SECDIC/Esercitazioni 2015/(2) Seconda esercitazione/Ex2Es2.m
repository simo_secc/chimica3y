% Esercizio (2) della seconda esercitazione di SECDIC
clc
clear all 
close all

global F m Tin2 Q cp

% Dati:
Tin=300; % [K]
Tin2=330; % [K]
cp=2.5; % [J/(mol K)]
m=100*1e3; % [mol]
F=8*1e3; % [mol/s]
Q=1e6; % [W]

% Risoluzione:
% Consideriamo innanzitutto il bilancio nella sua forma stazionaria, per
% determinare la temperatura uscente in caso stazionario (ci servir� da
% condizione al contorno nel caso in cui vi � il salto a gradino)

Ts=Tin+Q/(F*cp);
disp(['La temperatura iniziale del serbatoio � ',num2str(Ts),' [K]'])
% Ammettiamo ora che avvenga la variazione a gradino della temperatura:
% consideriamo l'evoluzione dinamica del sistema

T0=Ts;
options=odeset('RelTol',1E-8,'AbsTol',1E-12); 
tSpan=[0 100]; % [s^-1]
[t,T] = ode23s('fEx2Es2',tSpan,T0,options);

plot(t,T,'r-')
grid
axis([0 100 350 385])
xlabel('Tempo [s]')
ylabel('Temperatura di uscita [K]')
title('Andamento della T col tempo')