% Esercizio (3) della seconda esercitazione di SECDIC

clc
clear all
close all

% Dati:

F1=2; % [m^3/h]
F2=10; % [m^3/h]
C1=0.5; % [kmol/m^3]
C2=6; % [kmol/m^3]

% Risoluzione:

F3=F1+F2; % [m^3/h]
n3=F1*C1+F2*C2; % [mol/h]
C3=n3/F3; % [kmol/m^3]
disp(['Il valore assunto dalla concentrazione della corrente uscente � ',num2str(C3),' [kmol/m^3]'])

% Consideriamo ora la corrente 1 variabile:

tmax=20/0.04; % [s]
t=0:0.0001:tmax;
F1=0.04*t;

F3=F1+F2; % [m^3/h]
n3=F1.*C1+F2*C2; % [mol/h]
C3=n3./F3; % [kmol/m^3]


plot(t,C3)
xlabel('Tempo [s]')
grid
ylabel('Concentrazione [kmol/m^3]')
title('Andamento della concentrazione con il tempo')