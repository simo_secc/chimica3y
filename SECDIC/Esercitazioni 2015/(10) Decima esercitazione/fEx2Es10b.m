function dhdt=fEx2Es10b(t,h)

global Fin Cd Af g A hss2 tnewhss Kc Foutss

if t<tnewhss
    % stato stazionario
    Fout=Foutss;
else
    % nuovo setpoint (problema di servomeccanismo)
    epsilon=hss2-h;
    Fout=Foutss+Kc*epsilon;
    Foutmin=0;
    Foutmax=Af*Cd*sqrt(2*g*h);
    Fout=max(Fout,Foutmin);
    Fout=min(Fout,Foutmax);
end
dhdt=(Fin-Fout)/A;
end