%% Risoluzione dell'esercitazione (10) di SECDIC: Controllore proporzionale implementato su un serbatoio

clc
clear all
close all

global Fin Cd Af g A hss hss2 tnewhss Kc Foutss

% Dati:
A=5; % [m^2]
Cd=0.61;
Dout=18e-2; % [m]
Fin=0.1; % [m^3/s]
h0=4; % [m]
g=9.81; % [m/s^2]

% Risoluzione:

% Punto (1):

Af=pi*Dout^2/4;

% Parte algebrica: Fin=Fout; Fin=Aforo*Cd*sqrt(2*g*h)

hstaz=((Fin/(Cd*Af))^2)/(2*g);

disp(['La altezza del serbatoio nel caso stazionario � pari a ',num2str(hstaz),' [m]'])

% Consideriamo il bilancio materiale sul serbatoio.
% dmdt=roFin-roFout; dVdt=Fin-Fout; dhdt=(Fin-Fout)/A;

options=odeset('RelTol',1E-8,'AbsTol',1E-12); 
tSpan=0:0.1:2000; % [s]
[t,h]=ode45('fEx1Es10',tSpan,h0,options);


figure('Name','Andamento di h','NumberTitle','off')
plot(t,h)
grid on
legend('h')
xlabel('Tempo [s]')
ylabel('Livello del serbatoio [m]')
title('Andamento della h con il tempo')

% Punto (2)

% Ipotizziamo che la transizione al nuovo stato stazionario avvenga
% all'istante t=200 [s]
tnewhss=200; % [s]
Foutss=Cd*Af*sqrt(2*g*hstaz);
Kc=-1;
hss=hstaz;
hss2=2*hss;

options=odeset('RelTol',1E-8,'AbsTol',1E-12); 
z=0.1;
tSpan=0:z:300; % [s]
[t,h]=ode45('fEx2Es10a',tSpan,hss,options);


figure('Name','Andamento di h senza limiti fisici: nuovo setpoint e controllore proporzionale','NumberTitle','off')
plot(t,h)
grid on
legend('h')
xlabel('Tempo [s]')
ylabel('Livello del serbatoio [m]')
title('Andamento della h con il tempo')

epsilon=hss2-h;
Fout=Foutss+Kc.*epsilon;

for i=1:1:(tnewhss/z)
    Fout(i)=Foutss;
end

for i=(tnewhss/z):1:length(t)
    Fout(i)=Fout(i);
end

figure('Name','Andamento di Fout senza limiti fisici: nuovo setpoint e controllore proporzionale','NumberTitle','off')
plot(t,Fout)
grid on
legend('Fout')
xlabel('Tempo [s]')
ylabel('Portata uscente [m^3/s]')
title('Andamento della Fout con il tempo')

options=odeset('RelTol',1E-8,'AbsTol',1E-12); 
z=0.1;
tSpan=0:z:400; % [s]
[t,h]=ode45('fEx2Es10b',tSpan,hss,options);


figure('Name','Andamento di h con limitazioni fisiche: nuovo setpoint e controllore proporzionale','NumberTitle','off')
plot(t,h)
grid on
legend('h')
xlabel('Tempo [s]')
ylabel('Livello del serbatoio [m]')
title('Andamento della h con il tempo')

epsilon=hss2-h;
Fout=Foutss+Kc.*epsilon;
Foutmin=0;
Foutmax=Af*Cd.*sqrt(2*g.*h);

for i=1:1:(tnewhss/z)
    Fout(i)=Foutss;
end

for i=(tnewhss/z):1:length(t)
    Fout(i)=max(Fout(i),Foutmin);
    Fout(i)=min(Fout(i),Foutmax(i));
end

figure('Name','Andamento di Fout con limiti fisici: nuovo setpoint e controllore proporzionale','NumberTitle','off')
plot(t,Fout)
grid on
legend('Fout')
xlabel('Tempo [s]')
ylabel('Portata uscente [m^3/s]')
axis([0 400 -0.01 0.11])
title('Andamento della Fout con il tempo')
