function dcdt=fEx2Es1(t,c)
%c(1)=ca c(2)=cb c(3)=cc
global k1 k2
dcdt=[-k1*c(1); k1*c(1)-k2*c(2); k2*c(2)];