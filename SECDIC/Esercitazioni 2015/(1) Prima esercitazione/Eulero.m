%% Risoluzione di un'equazione differenziale mediante metodo di Eulero:
clc
clear all
close all

% Dati:
lambda=1;
t0=0;
t1=20;
h=1e-6;
t=t0:h:t1;
y(1)=5;
for i=1:length(t)
    dy(i)=-lambda*y(i);
    y(i+1)=y(i)+h*dy(i);
end
i=length(t);
disp(['Il valore assunto da y dopo 20 secondi � pari a ',num2str(y(i))])
