%% Risoluzione dell'esercitazione (6) di SECDIC

clc
clear all
close all

global Fin A epsilon h0 hlin i

%% Dati:

Fin=0; % [m^3/s]
h0=7; % [m]
g=9.81; % [m/s^2]
A=3; % [m^2]
epsilon=0.8*sqrt(2*g);

%% Risoluzione:

% Punto 1): risoluzione numerica

options=odeset('RelTol',1E-8,'AbsTol',1E-12); 
tSpan=0:0.0001:5; % [s]
[t,h]=ode15s('fEx1Es6',tSpan,h0);


i=1;
while h(i)>0
    i=i+1;
end
tsnum=t(i-1);


% Punto 2): risoluzione analitica
% A*dhdt=Fin-epsilon*sqrt(h);
% dhdt/sqrt(h)=-epsilon/A;
% int(dh/sqrt(h)) (valutato tra h0 e h) = int(-epsilon/A*dt) (valutato tra
% 0 e t)
% 2*h^(1/2)-2*h0^(1/2)=-epsilon/A*t;
% h=(-(epsilon/A*t)/2+h0^(1/2))^2;

tan=0:0.0001:5;
han=(-((epsilon/A).*tan)/2+h0^(1/2)).^2;

a=1;

while han(a+1)-han(a)<0
    a=a+1;
end

for j=1:a
hanplot(j)=han(j);
tanplot(j)=tan(j);
end

% Determiniamo il tempo analitico di svuotamento del serbatoio:

tsan=2*h0^(1/2)/(epsilon/A);

figure('Name','Andamento di h','NumberTitle','off')
plot(t,h,'-r',tanplot,hanplot,'-b')
grid on
legend('h numerico','h analitico')
xlabel('Tempo [s]')
ylabel('Livello del serbatoio [m]')
title('Andamento della h con il tempo')


% Secondo punto: tale punto prevede nuovamente la risoluzione
% dell'equazione differenziale, ma in forma discretizzata, inserendo la
% risoluzione dell'equazione differenziale all'interno di un ciclo while.

% In questo modo possiamo scegliere noi fino a quando far proseguire la
% risoluzione della equazione. Visto che ci interessa determinare dopo quanto tempo
% raggiungiamo lo stato stazionario (serbatoio vuoto), decidiamo un tempo
% di integrazione arbitrario (corrispondente a dFin), e valutiamo quanto
% varia la h del serbatoio in tale intervallo di tempo. Se la sua
% variazione � bassa, possiamo ritenere di essere vicini allo stato
% stazionario, e possiamo dunque bloccare il processo di integrazione.
% Scegliamo anche di inserire un contatore, per evitare di entrare in un
% loop infinito (sostanzialmente, anche se non abbiamo raggiunto la
% precisione richiesta, decidiamo di fermarci dopo un tempo prestabilito,
% in cui assumiamo di essere vicini allo stato stazionario).
dFin=0.1;
tSpan=0:0.01:dFin;
epsi=1e-3; 
% Corrisponde alla precisione da noi accettata: quando la differenza tra il 
% valore di h della precedente integrazione e il valore di h dell'integrazione 
% che sta avvenendo � minore di questo valore, vuol dire che ci stiamo 
% avvicinando allo stato stazionario, e possiamo dunque ritenerci soddisfatti 
% e fermarci
deltaH=1;
% E' un valore che serve a dare inizio al ciclo iterativo
counter=0;
% Corrisponde al contatore da noi usato per evitare il loop infinito, e ci
% servit� anche per plottare il tempo di integrazione
hVett=h0;
tVett=0;
% Corrispondono alle condizioni iniziali: prima di iniziare l'integrazione,
% l'altezza � pari a 7 [m] e il tempo � nullo. Tali vettori si
% modificheranno poi man mano nella risoluzione dell'equazione
% differenziale

while (deltaH>=epsi) && (counter<43)
    % Corrispondono alle condizioni che ci fanno rimanere nel ciclo while:
    % la prima � relativa alla precisione da noi voluta, la seconda � utile
    % per non entrare in un loop infinito
    [t,h]=ode23s('fEx1Es6',tSpan,h0,options);
    % Integrazione dell'equazione differenziale: nel primo passaggio,
    % l'altezza del serbatoio � 7 [m], e scegliamo di integrare per un
    % tempo di 0.1 secondi. Finita l'integrazione, otterremo un nuovo
    % valore dell'altezza del serbatoio (vedi punti successivi). E'
    % importante notare che non � necessario modificare ogni volta tSpan:
    % sarebbe necessario se il tempo (variabile indipendente) comparisse in
    % forma esplicita nell'equazione differenziale. Visto che cos� non �,
    % possiamo lasciare il tempo come vettore che va da 0 a 0.1.
    % Altrimenti, ad esempio, dovremmo fare in modo che nella seconda
    % integrazione il tempo vada da 0.2 a 0.3, e cos� via per le successive
    % integrazioni. Per farlo, � sufficiente inserire tSpan all'interno del
    % ciclo while, e creare una dipendenza con counter.
    hFin=h(length(h));
    % Imponiamo che l'altezza raggiunta dal serbatoio alla fine
    % dell'integrazione considerata corrisponde all'ultima del vettore h
    deltaH=abs(h0-hFin);
    % Calcoliamo il nuovo valore di deltaH, che corrisponde alla
    % differenza tra il valore di h prima della nuova integrazione e dopo
    % la nuova integrazione. Come detto in precedenza, ci� ci serve per
    % determinare se rimanere nel o uscire dal ciclo while
    hVett=[hVett hFin];
    % Visto che � nostro interesse plottare la funzione h(t), per vedere il
    % suo andamento, dopo ogni integrazione aggiungiamo al vettore "altezza
    % del serbatoio" il valore raggiunto alla fine dell'integrazione
    % stessa.
    h0=hFin;
    % Modifichiamo per ogni ciclo while il valore di h0: infatti,
    % l'integrazione successiva a quella appena avvenuta, non dovr� partire
    % dal valore iniziale del fluido del serbatoio, ma da quello appena
    % raggiunto (altrimenti analizzeremmo la dinamica del sistema sempre
    % nei primi 0.1 [s], mentre � nostro interesse valutare la dinamica
    % successiva)
    counter=counter+1;
    % Facciamo variare il valore del contatore, per il problema del loop
    % infinito e per plottare il tempo
    tVett=[tVett dFin*counter];
    % Visto che il tempo di integrazione � sempre tra 0 e 0.1 secondi, a
    % tVett dell'integrazione precedente aggiungiamo l'ampiezza
    % dell'intervallo di integrazione moltiplicata per il passaggio che
    % stiamo effettuando (espresso dal counter). Nella situazione iniziale, tVett vale 0. Dopo la
    % prima integrazione, siamo arrivati ad un tempo pari a 0.1*1, e
    % aggiungiamo dunque 0.1 al vettore del tempo. Dopo la seconda
    % integrazione, saremo arrivati ad un tempo pari a 0.1*2, e cos� via,
    % fino a quando non si arriva al termine della risoluzione
    % dell'equazione differenziale
end

figure('Name','Andamento di h (2): discretizzazione del tempo di integrazione','NumberTitle','off')
plot(tVett,hVett)
grid on
legend('h')
xlabel('Tempo [s]')
ylabel('Livello del serbatoio [m]')
title('Andamento della h con il tempo')

% Terzo punto: risoluzione dell'equazione differenziale con linearizzazione
% (mediante polinomio di Taylor arrestato al primo ordine)

% L'equazione differenziale da risolvere � del tipo dhdt=-epsilon*sqrt(h).
% La variabile di integrazione compare con potenza 1/2. Espandiamo tale
% potenza in serie di Taylor, nell'intorno del punto hlin (scelto con criterio dall'utente):
% sqrt(h)=(1/2)*(hlin^(-(1/2)))*(h-hlin). Sostituiamo quindi questa espressione
% nella function e risolviamo l'equazione differenziale


h0=7; % [m]
hlin=[2 1 0.1]; % [m]
g=9.81; % [m/s^2]

% Risoluzione dell'equazione differenziale non linearizzata


tSpan=0:0.0001:5; % [s]
[t1,h1]=ode15s('fEx1Es6',tSpan,h0);

% Risoluzione dell'equazione differenziale linearizzata
tSpan=0:0.0001:10; % [s]
i=1;
[t2,h2]=ode15s('fEx3Es6',tSpan,h0);
i=2;
[t3,h3]=ode15s('fEx3Es6',tSpan,h0);
i=3;
[t4,h4]=ode15s('fEx3Es6',tSpan,h0);

figure('Name','Andamento di h (3): caso non linearizzato e caso linearizzato','NumberTitle','off')
plot(t1,h1,'-r',t2,h2,'-b',t3,h3,'-g',t4,h4,'-m')
legend('h non linearizzata','h linearizzata (hlin=2)','h linearizzata (hlin=1)','h linearizzata (hlin=0.1)')
grid on
xlabel('Tempo [s]')
ylabel('Livello del serbatoio [m]')
title('Andamento della h con il tempo')

clc
disp('---------- Primo punto ----------')
disp(['Il tempo necessario (numerico) allo svuotamento del serbatoio � pari a ',num2str(tsnum),' [s]'])
disp(['Il tempo necessario (analitico) allo svuotamento del serbatoio � pari a ',num2str(tsan),' [s]'])