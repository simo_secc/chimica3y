function dAdt=f2Ex1Es4(t,A)

% A(1)=h ; A(2)=T

global Fin Fout D Tin U ro cp Tamb Abase
dAdt=[(Fin-Fout)/Abase ; (ro*Fin*cp*(Tin-A(2))-U*pi*D*A(1)*(A(2)-Tamb))/(ro*pi*D^2/4*A(1)*cp)];
