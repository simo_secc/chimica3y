%% Esercitazione (11) di SECDIC : Controllore PI implementato su un serbatoio riscaldato

clc
clear all
close all

global FvSS FvSS2 Kc taui Tin tau DHevm ro V cp tcambioSP Tout2 integrale 

% Dati:
Tin=273.15+30; % [K]
DHev=40.65e3; % [J/mol]
PMH2O=18e-3; % [kg/mol]
ro=1000; % [kg/m^3]
DHevm=DHev/PMH2O; % [J/kg]
% Le portate entranti e uscenti dal serbatoio risultano essere le stesse,
% in quando consideriamo il serbatoio in condizioni stazionarie (dhdt=0)
V=5; % [m^3]
tau=2*60; % [s], � pari al rapporto tra V e Fin=Fout
Tout=273.15+70; % [K]
cp=4186; % [J/(kg*K)]
Kc=0.001;
taui=30; % [s]

% Risoluzione:

% Punto (1):
% Determinazione della portata massiva di vapore che garantisca
% le condizioni di uscita (stato stazionario).
% Sviluppiamo come segue la scrittura del bilancio energetico sul serbatoio
% riscaldato:
% dHdt=Hin-Hout+FvDHev
% ro*cp*ddt(V(T-T*))=ro*Fin*cp*(Tin-T*)-ro*Fout*cp*(T-T*)+FvDHev
% dTdt=(Tin-T)/tau+DHev*Fv/(ro*V*cp)

FvSS=((Tout-Tin)/tau)*(ro*V*cp)/DHevm;

disp(['Il valore assunto dalla portata massiva di vapore � pari a ',num2str(FvSS),' [kg/s]'])

% Punto (2):
% Implementiamo ora un controllore PI alla portata di vapore, con lo scopo
% di risolvere un servo problem: all'istante t=10 [s], scegliamo di
% modificare il setpoint della T di uscita da 70 [�C] a 90 [�C]

% Metodo (a): Lasciamo che la routine di calcolo determini da sola il
% valore dell'integrale

Tout2=273.15+90; % [K]
FvSS2=((Tout2-Tin)/tau)*(ro*V*cp)/DHevm;
options=odeset('RelTol',1E-8,'AbsTol',1E-12); 
z=0.1;
tSpan=0:z:100000; % [s]
tcambioSP=200; % [s]
[t,y]=ode45('fEx2aEs11',tSpan,[Tout 0],options);

T=y(:,1);
integrale=y(:,2);
figure('Name','Andamento di T','NumberTitle','off')
plot(t,T)
grid on
legend('T')
xlabel('Tempo [s]')
ylabel('Temperatura del CSTR [K]')
title('Andamento della T con il tempo')
figure('Name','Andamento di integrale','NumberTitle','off')
plot(t,integrale)
grid on
legend('integrale')
xlabel('Tempo [s]')
ylabel('Valore di integrale')
title('Andamento di integrale con il tempo')

epsi=Tout2-T;

Fv=zeros(1,length(t));

for i=1:(tcambioSP/z)
    Fv(i)=FvSS;
end

for i=(tcambioSP/z):length(t)
    Fv(i)=FvSS+Kc*epsi(i)+Kc/taui*integrale(i);
end
 
figure('Name','Andamento di Fv','NumberTitle','off')
plot(t,Fv)
grid on
legend('Fv')
xlabel('Tempo [s]')
ylabel('Potata di vapore [kg/s]')
title('Andamento della Fv con il tempo')

% % Metodo (b): Calcoliamo il valore dell'integrale mediante la routine di
% % calcolo BraccobaldoBau
% 
% options=odeset('RelTol',1e-8,'AbsTol',1e-12,'OutputFcn',@BraccobaldoBau);
% [t,y]=ode113('fEx2bEs11',tSpan,Tout,options);
% 
% T=y(:,1);
% integrale=y(:,2);
% figure('Name','Andamento di T (2)','NumberTitle','off')
% plot(t,T)
% grid on
% legend('T')
% xlabel('Tempo [s]')
% ylabel('Temperatura del CSTR [K]')
% title('Andamento della T con il tempo')
% figure('Name','Andamento di integrale (2)','NumberTitle','off')
% plot(t,integrale)
% grid on
% legend('integrale')
% xlabel('Tempo [s]')
% ylabel('Valore di integrale')
% title('Andamento di integrale con il tempo')
% 
% epsi=Tout2-T;
% 
% Fv=zeros(1,length(t));
% 
% for i=1:(tcambioSP/z)
%     Fv(i)=FvSS;
% end
% 
% for i=(tcambioSP/z):length(t)
%     Fv(i)=FvSS2+Kc*epsi(i)+Kc/taui*integrale(i);
% end
%  
% figure('Name','Andamento di Fv (2)','NumberTitle','off')
% plot(t,Fv)
% grid on
% legend('Fv')
% xlabel('Tempo [s]')
% ylabel('Potata di vapore [kg/s]')
% title('Andamento della Fv con il tempo')