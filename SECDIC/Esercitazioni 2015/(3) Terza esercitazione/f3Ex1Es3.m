function dhdt=f3Ex1Es3(t,h)
global Fin Fout Abase H

if h<H && h>0
dhdt=(Fin-Fout)/Abase;
else
    dhdt=0;
end