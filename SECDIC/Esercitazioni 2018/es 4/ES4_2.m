%Serbatoio, caso generale, si presta alla variazione dei parametri

close all
clear all
clc

L0 = 0.5; %livello iniziale
D = 6; %diametro
Fi = 10; %portata in
Fo = 10; %portata out
H = 4; %altezza

%faccio due casi, A e B, per i diversi dati del testo

handA = @(t,L)serbA(t,L,D,Fi,Fo); % <-- N.B. uso handle come descritto sul foglio
[tA,LA] = ode45(handA, [0 20], L0); %integro

handB = @(t,L)serbB(t,L,D,Fi,Fo);
[tB,LB] = ode45(handB, [0 20], L0);

Dnuovo = 22; %cambio il diametro
handC = @(t,L)serbC(t,L,Dnuovo,Fi,Fo);
[tC,LC] = ode45(handC, [0 20], L0);

figure(1)
plot(tA,LA)                             %grafico
title('Riduzione portata uscente')
figure(2)
plot(tB,LB)
title('Riduzione portata entrante')
figure(3)
hold on
plot(tC,LC)
plot(tB,LB)
title('Confronto a diametri diversi')


function Lp = serbA(t,~,D,Fi,Fo)                 %N.B.--> se la funzione � scritta qui, solo questo script la vede, se invece � in un file a parte, tutti gli script della cartella la vedono

if t > 5
    Fo = 0;
end

Lp = (Fi-Fo)/(pi/4*D^2); 

end

function Lp = serbB(t,~,D,Fi,Fo)                 

if t > 5
    Fi = 5;
end

Lp = (Fi-Fo)/(pi/4*D^2);

end

function Lp = serbC(t,~,D,Fi,Fo)                 

if t > 5
    Fi = 5;
end

Lp = (Fi-Fo)/(pi/4*D^2);

end