%Serbatoio in stato stazionario

close all
clear all
clc

L0 = 0.5; %livello iniziale

[t,L] = ode45(@serb, [0 20], L0); %integro

plot(t,L) %grafico


function Lp = serb(~,~)

Lp = 0; %bilancio per stato stazionario

end