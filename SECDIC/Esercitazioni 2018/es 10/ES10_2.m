%Serbatoio riscaldato con controllore PI
close all
clear all
clc

global Fvvett

%Dati
V = 5; %[m3]
t_res = 2*60; %[s]
Ti = 30; %[�C]
Tss = 70; %[�C]
PM = 18; %[g/mol]
DHev = 40.65/PM*1000; %[kJ/kg] <-- N.B. unit� di misura
ro = 1000; %[kg/m3]
F = V/t_res; %[m3/s]
cp = 4.186; %[kJ/(kg*K)]
Fvss = ro*F*cp*(Tss-Ti)/DHev; %[kg/s]
kc = 1;
tauI = 30;

%Integrazione
hand = @(t,y)scalda(t,y,ro,F,cp,Ti,Fvss,DHev,V,kc,tauI);
handout = @(t,T,flag)Fvout(t,T,flag,Fvss,kc,tauI);
opt = odeset('OutputFcn',handout,'Refine',1,'AbsTol',1e-8,'RelTol',1e-12);
[t,y] = ode45(hand,[0 600],[30 0],opt); %Tiniziale = 30�C  N.B. --> c'� valore iniziale (0) anche per integrale, che � y(2)

figure(1)
subplot(2,1,1)
hold on
plot(t,y(:,1))
plot(t,ones(1,length(t))*70,'--') %� un modo per disegnare la retta del sp
plot(t,ones(1,length(t))*90,'--') %uguale a sopra
xlabel('t[s]')
ylabel('T[�C]')
title('Andamento temperatura')
subplot(2,1,2)
plot(t,Fvvett,'r')
xlabel('t[s]')
ylabel('Fv[kg/s]')
title('Portata di vapore')

function yp = scalda(t,y,ro,F,cp,Ti,Fvss,DHev,V,kc,tauI)
if t <= 200
    Tsp = 70;
else
    Tsp = 90;
end

T = y(1);
int = y(2);

epsi = Tsp - T;
Fv = Fvss + kc*epsi + kc/tauI*int;

yp(1,1) = (ro*F*cp*(Ti-T) + Fv*DHev)/(ro*V*cp);
yp(2,1) = epsi; %come nelle slide
end

function status = Fvout(t,y,flag,Fvss,kc,tauI)
global Fvvett
status = 0;
if strcmp(flag,'')
    if t <= 200
     Tsp = 70;
    else
     Tsp = 90;
    end
    epsi = Tsp - y(1);
    Fv = Fvss + kc*epsi + kc/tauI*y(2);
    Fvvett = [Fvvett Fv]; % <-- N.B. scrivere il vettore cos�
elseif strcmp(flag,'init')
    Fvvett = Fvss;
end
end