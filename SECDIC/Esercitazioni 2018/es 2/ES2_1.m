%Funzione di crescita batterica, con diversi integratori, poi posso anche
%provare a variare la tollesranza

close all
clear all
clc

hold on

[t,y] = ode23s(@battgr, [0 15], [1 1], odeset('AbsTol',1e-10,'RelTol',1e-10)); %N.B. --> ho aumentato la tolleranza
plot(t,y)

%funzione
function yp = battgr(~,y)
k1 = 0.5;
k2 = 1*10^(-7);
k3 = 0.6;

yp(1,1) = k1*y(1)*y(2)/(k2+y(2));
yp(2,1) = -k3*k1*y(1)*y(2)/(k2+y(2));
end