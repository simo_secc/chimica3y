%Serbatoio miscelato e scaldato

close all
clear all
clc

[t,T] = ode23(@tank, [0 300], 300);
plot(t,T)

function Tp = tank(t,T)

n = 100;
F = 8;
Q = 1e3;
cp = 2.5;

if t >= 150              %gradino
    Tin = 300 + 150;
else
    Tin = 300;
end
    
Tp = (F*cp*(Tin-T) + Q)/(n*cp);
end