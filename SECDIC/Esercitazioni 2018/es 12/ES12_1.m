%Serbatioi interagenti,Cohen-Coon

close all
clear all
clc

%Dati
A1 = 30; %[m2]
A2 = 50; %[m2]
r1 = 1.2; %[s/m2]
Fi = 9.4; %[m3/s]
hss2 = Fi/1.43;    %stazionario
hss1 = Fi*r1+hss2;

%Integro open loop
hand = @(t,h)openloop(t,h,Fi,r1,A1,A2);
[t,h] = ode45(hand,[0 400], [hss1 hss2]);

plot(t,h(:,2))

%Funzione open loop con disturbo a gradino
function hp = openloop(t,h,Fi,r1,A1,A2)

if t >= 60
    Fi = 2*Fi;  %disturbo a gradino
end

F1 = (h(1)-h(2))/r1;
Fo = 1.43*h(2);

hp(1,1) = (Fi-F1)/A1;
hp(2,1) = (F1-Fo)/A2;
end