%B.E. in serbatoio senza scambi di massa

close all
clear all
clc

D = 0.5; %[m]
L = 1; %[m]
T0 = 25; %[�C]
Q = 5000; %[W]
ro = 1000; %[kg/m3]
cp = 4186; %[J/kg/K]
m = ro*L*(D^2)*pi/4; %[kg]

hand = @(t,T)serb(Q,m,cp);        %N.B. --> handle usato come nella nota a ES4
[t,T] = ode45(hand, [0 3600], T0);

plot(t,T)   %il tempo � un secondi
xlabel('t[s]')
ylabel('T[�C]')

function Tp = serb(Q,m,cp)  %non metto le variabili che non servono, come scritto nella nota a ES4
Tp = Q/(m*cp);
end