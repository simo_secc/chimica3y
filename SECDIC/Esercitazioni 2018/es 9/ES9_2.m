%Simulazione controllore P

close all
clear all
clc

%Dati
Fi = 9.4;
A1 = 30;
r = 1.2;
A2 = 50;
h0 = 3;
hsp = 6.6;
kc = -10;

%Integrazione
hand = @(t,h)tanks(t,h,A1,A2,Fi,r,hsp,kc);
opt = odeset('RelTol',1e-8,'AbsTol',1e-8);
[t,h] = ode45(hand,[0 60*10],[h0 h0],opt);

hspplot = zeros(1,length(t));
for i = 1:length(t)
    hspplot(i) = hsp;
end
hold on
plot(t,h(:,1))
plot(t,h(:,2))
plot(t,hspplot,'--')
legend('serb. 1','serb. 2','set point')

function hp = tanks(t,h,A1,A2,Fi,r,hsp,kc)

if t > 5*60
    Fi = 2*Fi;
end

F1 = (h(1)-h(2))/r;
Fo = Fi + kc*(hsp-h(2));          %cambia Fo

hp(1,1) = (Fi-F1)/A1;
hp(2,1) = (F1-Fo)/A2;

end