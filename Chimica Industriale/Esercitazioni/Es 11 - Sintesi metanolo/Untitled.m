clear all
clc

global P DHr v d cpi

%[CO H2 CH3OH CH4]
Fo=[2000 9500 0 1950]/60 %kmol/min
To=523

v=[-1 -2 1 0];
cpi=[7.2 8.5 14.7 11.5]; %cal/molK
d=713;
P=51; %atm
DHr=-23460; %cal/mol

[t,y]=ode23('sistema',[0 0.29],[Fo To]);

subplot(2,2,1)
plot(t,y(:,1))
xlabel('V [m^3]');
ylabel('F CO [kmol/min]');
grid on
subplot(2,2,2)
plot(t,y(:,2))
xlabel('V [m^3]');
ylabel('F H2 [kmol/min]');
grid on
subplot(2,2,3)
plot(t,y(:,3))
xlabel('V [m^3]');
ylabel('F CH3OH [kmol/min]');
grid on
subplot(2,2,4)
plot(t,y(:,5))
xlabel('V [m^3]');
ylabel('T [K]');
grid on

Fout=y(length(y),1:4)
Tout=y(length(y),5)