clear all
clc

global Tc Pc om tipo T P Fin phi K v

%1.5H2+0.5N2-->NH3
v=[-1.5 -0.5 1 0 0];
%[H2 N2 NH3 CH4 Ar]
Tc=[33.2 126.2 405.6 190.6 150.8];
Pc=[12.8 33.5 111.3 45.4 48.1]*101325;
om=[-0.22 0.04 0.25 0.008 -0.004];
tipo=3;

T=773;
P=275*101325;
Fin=[0.626 0.2087 0.0353 0.0978 0.0322]*470000/(22.414*3600); %kmol/s

[Z,A,B,S,k]=EoS(T,P,Tc(1),Pc(1),om(1),tipo);
phi(1)=phi_puri(Z(3),A,B,tipo);

[Z,A,B,S,k]=EoS(T,P,Tc(2),Pc(2),om(3),tipo);
phi(2)=phi_puri(Z(3),A,B,tipo);

[Z,A,B,S,k]=EoS(T,P,Tc(3),Pc(3),om(3),tipo);
phi(3)=phi_puri(Z(3),A,B,tipo);
phi(4)=1;
phi(5)=1;

Kphi=phi(3)/(phi(1)^1.5*phi(2)^0.5);
K=exp((12972-27.84*T)*4.186/(8.314*T))

options=optimoptions('fsolve','Tolfun',1e-10,'Maxiter',3e3,'MaxFunEvals',20000);

disp('[H2 N2 NH3 CH4 Ar]');
disp('miscela ideale di gas reali: ');
lambda=fsolve('miscelaideale',0.7,options)
F=Fin+lambda*v;
y=F/sum(F)

disp('[H2 N2 NH3 CH4 Ar]');
disp('miscela reale di gas reali: ');
lambda=fsolve('miscelareale',0.7,options)
F=Fin+lambda*v;
y=F/sum(F)