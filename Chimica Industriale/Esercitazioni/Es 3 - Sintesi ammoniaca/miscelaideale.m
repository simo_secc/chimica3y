function f=miscelaideale(lambda)

global P Fin phi K v

F=Fin+lambda*v;
y=F/sum(F);
ai=(P/101325)*phi.*y;

f=K-prod(ai.^v);