function phi=phi_puri(Z,A,B,tipo)

%phi_puri calcola il coeff. di fugacit� di un composto puro con EoS

if tipo == 1		%vdW
    phi = exp(Z-1-A/Z-log(Z-B));
end

if tipo == 2 	%RK
   phi = exp(Z-1-A/B*log((Z+B)/Z)-log(Z-B));
end

if tipo == 3 	%RKS
   phi = exp(Z-1-A/B*log((Z+B)/Z)-log(Z-B));
end

if tipo == 4	%PR
   phi = exp(Z-1-A/(2*sqrt(2)*B)*log((Z+B*(1+sqrt(2)))/(Z+B*(1-sqrt(2))))-log(Z-B));
end