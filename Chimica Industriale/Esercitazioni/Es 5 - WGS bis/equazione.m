function f=equazione(T)

global Tin cpai cpbi cpci cpdi cpei Fin lambda Dh


I=quad(@(t) (cpai*Fin')+(cpbi*Fin')*t+(cpci*Fin')*t.^2+(cpdi*Fin')*t.^3+(cpei*Fin')*t.^-2,Tin,T);

f=I+lambda*Dh;