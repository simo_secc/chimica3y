clear all
clc

Tin=823;
Tout=1073;

%[CH4 H2O CO CO2 H2]
Dhf=[-74.52 -241.8 -110.53 -393.51 0]*1e3;
cpia=[4.568 4.395 3.912 3.259 2.883]*8.314;
cpib=[-8.975 -4.186 -3.913 1.356 3.681]*8.314e-3;
cpic=[3.631 1.405 1.182 1.502 -0.772]*8.314e-5;
cpid=[-3.407 -1.564 -1.302 -2.374 0.692]*8.314e-8;
cpie=[1.091 0.632 0.515 1.056 -0.213]*8.314e-11; %J/molK

Fin=[22486 94015 0 0 0]*1/(3600*22.414); %kmol/s
v=[-1 -1 1 0 3;
    0 -1 -1 1 1];

lambda1=0.7*Fin(1)
lambda2=2.948/22.414 %kmol/s

Dho=Dhf*v'

Dh(1)=Dho(1)+quad(@(t) (v(1,:)*cpia')+(v(1,:)*cpib')*t+(v(1,:)*cpic')*t.^2+(v(1,:)*cpid')*t.^3+(v(1,:)*cpie')*t.^4, 298,Tout);
Dh(2)=Dho(2)+quad(@(t) (v(2,:)*cpia')+(v(2,:)*cpib')*t+(v(2,:)*cpic')*t.^2+(v(2,:)*cpid')*t.^3+(v(2,:)*cpie')*t.^4, 298,Tout)


Q=quad(@(t) (Fin*cpia')+(Fin*cpib')*t+(Fin*cpic')*t.^2+(Fin*cpid')*t.^3+(Fin*cpie')*t.^4, Tin,Tout)+lambda1*Dh(1)+lambda2*Dh(2)