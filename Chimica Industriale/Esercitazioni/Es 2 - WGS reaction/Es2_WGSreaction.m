clear all
clc

global F1i T P K v

Q1=2400.2; %mol/s
Q1secco=1490.4;
F1=100; %mol/s normalizzata
F1i=[35 13.81 7.8 5.14 0.168 0.186 37.896]; 

T=543.15;
P=30; %atm
R=8.314;
%[H2 N2 CO CO2 Ar CH4 H2Ov]
Dgf=[0 0 -137.2 -394.4 0 -50.5 -228.6]*1e3; %298.15K, 1atm J/mol
Dhf=[0 0 -110.5 -393.5 0 -74.6 -241.8]*1e3;
cpai=[33.066178 19.50583 25.56759 24.99735 20.78600 -0.703029 30.09200]; %a+bT+cT^2+dT^3+eT^-2 J/molK
cpbi=[-11.363417 19.88705 6.096130 55.18696 2.825911e-7 108.4773 6.832514]*1e-3;
cpci=[11.432816 -8.598535 4.054656 -33.69137 -1.464191e-7 -42.52157 6.793435]*1e-6;
cpdi=[-2.772874 1.369784 -2.671301 7.948387 1.092131e-8 5.862788 -2.534480]*1e-9;
cpei=[-0.158558 0.527601 0.131021 -0.136638 -3.661371e-8 0.678565 0.082139]*1e6;

v=[1 0 -1 1 0 0 -1];

Dgo=Dgf*v'
Dho=Dhf*v';
Ko=exp(-Dgo/(R*298.15))

cpa=cpai*v';
cpb=cpbi*v';
cpc=cpci*v';
cpd=cpdi*v';
cpe=cpei*v';

syms t1 t
Ks=Ko*exp( int( (Dho+int(cpa+cpb*t1+cpc*t1.^2+cpd*t1.^3+cpe*t1.^-2,t1,298.15,t))/(R*t^2),t,298.15,T) );
K=eval(Ks)


lambda=fsolve('reazione',7)
F2i=F1i+lambda*v;
x2=F2i/sum(F2i)
x2secco=[x2(1) x2(2) x2(3) x2(4) x2(5) x2(6)]/(sum(x2)-x2(7))