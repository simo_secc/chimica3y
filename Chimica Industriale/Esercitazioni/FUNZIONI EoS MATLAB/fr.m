function [hrRT,srR]=fr(Z,A,B,E,tipo)
%
%fr calcola le funzioni residue con EoS cubiche: tipo 1 vdW
%                               tipo 2 RK        
%                               tipo 3 RKS            
%                               tipo 4 PR
% E=S*sqrt(TR/k)
%(I dati A,B,S sono calcolate con EoS) 
% restituisce hr/RT e sr/R

if tipo==1      %vdW
    hrRT=Z-1-A/Z;
    srR=log(Z-B);
end
if tipo==2      %RK
    hrRT=Z-1-3*A/(2*B)*log((Z+B)/Z);
    srR=log(Z-B)-A/(2*B)*log((Z+B)/Z);
end
if tipo==3      %RKS
    hrRT=Z-1-A/B*(1+E)*log((Z+B)/Z);
    srR=log(Z-B)-A*E/B*log((Z+B)/Z);
end
if tipo==4      %PR
    hrRT=Z-1-A/(2*sqrt(2)*B)*(1+E)*log((Z+B*(1+sqrt(2)))/(Z+B*(1-sqrt(2))));
    srR=log(Z-B)-A*E/(2*sqrt(2)*B)*log((Z+B*(1+sqrt(2)))/(Z+B*(1-sqrt(2))));
end