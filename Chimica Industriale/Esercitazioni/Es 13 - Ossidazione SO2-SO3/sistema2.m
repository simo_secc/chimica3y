function f=sistema2(t,X)

global P DHr cpi v  ko E Fin

conv=X(1);
T=X(2);

F=Fin+v*Fin(1)*conv;
Pi=P*F/sum(F);

k=ko.*exp(-E./(8.314*T));
R=(k(1)*Pi(1)*Pi(2)-k(2)*Pi(3)*Pi(2)^0.5)/(Pi(1)^0.5)*1000; %mol/kg*s

f=[R/Fin(1);
    -DHr*R/sum(F*cpi')];