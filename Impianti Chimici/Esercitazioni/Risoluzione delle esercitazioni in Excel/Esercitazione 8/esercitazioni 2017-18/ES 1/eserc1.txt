introduzione
Ore: con.
date: con /
formule: inizio con =

tasto dx su cella-->formato

riferirsi a un area: relativo (A4:B12) oppure ($B14) o ($B$14) assoluto per non farla variare quando la sposto
per visulaizzare la formula clicco 2 volte

funzioni di escel:
-somma totale: SOMMA(A1: A10)
-MAX(num1,num2,...) o MIN
-MEDIA
LN
PI.GRECO



errori:
#DIV/0! cella vuota
# NOME? nome funz sbagliato
#RIF! riferimenti celle sbagliato
#VALORE

azzeramento:
1)RICERCA OBBIETTIVO
dati-->analisi simulazione-->ricerca obbiettivo
imposta cella (funz)
al valore di (0)
cambiando (val hp)

2)RISOLUTORE (ottimizzazione) da aggiungere (componenti aggiuntivi)
T di 1a Hp-->pev a THp-->yi


ESERCIZIO 1
wi= (xi MWi)/MWmix   MWmix= sum(xi*MWi) -->richiamo la funz 'somma'(da cella: a cella)

per scrivere una formula inizio con =
poi seleziono le celle
poi premo invio

per ripetere la formula trascino (copia ci� che � a dx) con il + nero grassetto
doppioclic: vedo che operazione ha fatto

NB non posso trascinare quando ho una divisione perch� divido per PMmix che � IN UNA SOLA CELLA (la cella a dx � vuota) devo bloccarla
blocco la colonna B mettendo $ davanti a B per bloccare movimento colonna (davanti a 5 per bloccare righe)
trascinando ora, trascina solo i rif senza $

NB con copia incolla copi la formula

ESERCIZIO 2
xi=(wi*MWmix)/MWi

ESERCIZIO 3
Clausius Clapeyron ln(Pev[mmHg])=A/T[K] + B
prendo un range di valori per interpolare (T=asse x)
per convertire in K basta trascinare perch� la costante non cambia
per P blocco la riga ma non la T (F4 blocca tutto)

scelgo grafico dispersione
seleziona dati-->aggiungi-->serie
Antoine: ln(Pev)=A-B/(T+C)


T BOLLA 
p*yi=xi*Pevi(T) --> yi=xi*Pevi(T) /P

sum(Yi)=1--> Pb=sum(xi*Pevi(T))

T RUGIADA
xi=(P*yi)/Pevi(T)
sum(xi)=1 --> Pd=1/sum(yi/Pevi(T))
               Td: sum (yi/Pevi(T)) - 1/P =0

ESERCIZIO 9
sfrutto la tabella e poi uso Tbolla come somma

ESERCIZIO 16
ci risulta comodo scrivere una funz da azzerare (funz obbiettivo)
P-sum(xi*Pevi(T))=0
Dati, analisi simulaz, ricerca obbiettivo
imposto cella con funz obbiettivo, al valore 0 variando la T (seleziono valore di primo tentativo)
per sapere quando fermarsi : file, opzioni, formule, aggiungo 0 dopo la virgola e prima di 1
