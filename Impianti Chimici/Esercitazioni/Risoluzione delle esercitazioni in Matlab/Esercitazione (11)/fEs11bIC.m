function F=fEs11bIC(X)

global Dis L Ais hi Ai st lambda ro g beta nu Amlt Tp Ta Tv Pra

si=X;
Des=Dis+2*si;
Aes=pi*Des*L;
Amls=(Aes-Ais)/log(Aes/Ais);
U=(Aes/(hi*Ai)+st/lambda(3)*Aes/Amlt+si/lambda(4)*Aes/Amls)^(-1);
Gr=Des^3*ro(1)^2*g*beta*(Tp-Ta)/(nu(1)^2);
Nue=0.525*(Gr*Pra)^0.25;
he=Nue*lambda(1)/Des;

F=U*(Tv-Tp)-he*(Tp-Ta);