% Risoluzione dell'esercitazione (10) di I.C. : Dispersioni da una condotta
% di vapore (Mattia Usuelli)

global Des Dis L hi st Amlt beta g Ta Pra Ai

clc
clear all
close all

%% Dati:
Di=0.154; %[m]
De=0.168; %[m]
si=0.0375; %[m]
Ta=294.15; %[K]
PH2O=28*1e5; %[Pa]
v=5; %[m/s]

% Si consideri il vettore delle specie S=[aria vapore metalli coibente]', 0
% sta ad indicare che non si conoscono i dati:
nu=[1.967e-5 2.0413e-5 0 0]';
cp=[1007 2420 0 0]';
lambda=[0.0282 0.044 43.275 0.069]';
ro=[1.09 10.8445 0 0]';

% Tensione di vapore di H2O nella forma lnPv=A-B/(T+C), con T in [K] e Pv
% in [mmHg]
A=18.3036;
B=3816.44;
C=-46.13;

%% Risoluzione dell'esercitazione:

% 1) Determinazione del coefficiente globale di scambio, della temperatura
% di pelle e delle dispersioni

disp('----- Spessore di isolante noto e Tp incognita -----')

% Ammettiamo che il tubo sia lungo un metro
L=1;
Ai=pi*Di*L;
Ae=pi*De*L;
Amlt=(Ae-Ai)/log(Ae/Ai);
Dis=De;
Des=Dis+2*si;
Ais=pi*Dis*L;
Aes=pi*Des*L;
Amls=(Aes-Ais)/log(Aes/Ais);
Tv=(B/(A-log(PH2O/101325*760)))-C+85;
Rev=ro(2)*Di*v/nu(2);
Prv=nu(2)*cp(2)/lambda(2);
Nui=0.023*Rev^(0.8)*Prv^(0.3);
hi=Nui*lambda(2)/Di;
st=(De-Di)/2;
U=(Aes/(hi*Ai)+st/lambda(3)*Aes/Amlt+si/lambda(4)*Aes/Amls)^(-1);
Pra=nu(1)*cp(1)/lambda(1);
g=9.81;

% Considero una Tp (T di pelle) di primo tentativo:
Tp(1)=340;
Tfilm(1)=(Tp(1)+Ta)/2;
beta(1)=1/Tfilm(1);
Gr(1)=Des^3*ro(1)^2*g*beta(1)*(Tp(1)-Ta)/(nu(1)^2);
Nue(1)=0.525*(Gr(1)*Pra)^0.25;
he(1)=Nue(1)*lambda(1)/Des;
Tp(2)=(he(1)*Ta+U*Tv)/(he(1)+U);
i=2;

while abs(Tp(i)-Tp(i-1))>1e-20
Tfilm(i)=(Tp(i)+Ta)/2;
beta(i)=1/Tfilm(i);
Gr(i)=Des^3*ro(1)^2*g*beta(i)*(Tp(i)-Ta)/(nu(1)^2);
Nue(i)=0.525*(Gr(i)*Pra)^0.25;
he(i)=Nue(i)*lambda(1)/Des;
i=i+1;
Tp(i)=(he(i-1)*Ta+U*Tv)/(he(i-1)+U);
end

disp(['Il valore assunto dalla T di pelle � pari a ',num2str(Tp(i-1)),' [K]'])
disp(['Il valore assunto dalla T di film � pari a ',num2str(Tfilm(i-1)),' [K]'])
disp(['Il valore assunto da he � pari a ',num2str(he(i-1)),' [W/(m^2*K)]'])
disp(['Il valore assunto da Nue � pari a ',num2str(Nue(i-1))])

Utot=(1/U+1/he(i-1))^(-1);
dispersioni=Utot*Aes*(Tv-Ta);

disp(['Il valore assunto dalle dispersioni � pari a ',num2str(dispersioni),' [W]'])


