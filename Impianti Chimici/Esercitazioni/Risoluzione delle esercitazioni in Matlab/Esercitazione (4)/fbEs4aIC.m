function F=fbEs4aIC(X)

global AntA AntB AntC P xB

F=(sum((exp(AntA-AntB./(X+AntC))).*xB))-P*760;