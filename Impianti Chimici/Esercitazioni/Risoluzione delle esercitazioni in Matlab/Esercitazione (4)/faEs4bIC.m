function F=faEs4bIC(X)

global alfamedio xD teta1 teta2

% Con X(1) si intende la frazione molare di 2-metilesano nel distillato in
% caso di rapporto di riflusso minimo, mentre con X(2) si intende il
% rapporto di riflusso minimo

F(1)=(alfamedio(1)*xD(1))/(alfamedio(1)-teta1)+(alfamedio(2)*xD(2))/(alfamedio(2)-teta1)+(alfamedio(3)*X(1))/(alfamedio(3)-teta1)+(alfamedio(4)*xD(4))/(alfamedio(4)-teta1)+(alfamedio(5)*xD(5))/(alfamedio(5)-teta1)-X(2)-1;
F(2)=(alfamedio(1)*xD(1))/(alfamedio(1)-teta2)+(alfamedio(2)*xD(2))/(alfamedio(2)-teta2)+(alfamedio(3)*X(1))/(alfamedio(3)-teta2)+(alfamedio(4)*xD(4))/(alfamedio(4)-teta2)+(alfamedio(5)*xD(5))/(alfamedio(5)-teta2)-X(2)-1;
