function F=fbEs4bIC(X)

global Rmin xD2mRmin xD2mNmin

% La grandezza a corrisponde ad X(1), mentre la grandezza B corrisponde a
% X(2)

F(1)=xD2mRmin-X(1)-X(2)*(Rmin/(Rmin+1));
F(2)=xD2mNmin-X(1)-X(2);
