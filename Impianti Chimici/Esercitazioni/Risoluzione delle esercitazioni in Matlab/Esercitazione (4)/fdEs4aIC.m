function F=fdEs4aIC(teta)

global alfamedio z q

F=(sum((alfamedio.*z)./(alfamedio-teta)))-1+q;