function F=faEs4aIC(X)

global AntA AntB AntC P z

F=(sum((exp(AntA-AntB./(X+AntC))).*z))-P*760;