function F=fcEs4aIC(X)

global AntA AntB AntC P xD

F=(sum(((P*760).*xD./(exp(AntA-AntB./(X+AntC))))))-1;