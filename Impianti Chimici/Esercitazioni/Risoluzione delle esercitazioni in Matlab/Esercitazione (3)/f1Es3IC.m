function F=f1Es3IC(T)

global A B C P xb xt

F=(exp(A(1)-B(1)./(T+C(1))).*xb)+(exp(A(2)-B(2)./(T+C(2))).*xt)-P*760;