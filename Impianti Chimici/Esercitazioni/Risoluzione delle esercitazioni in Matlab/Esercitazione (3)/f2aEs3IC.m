function F=f2aEs3IC(T)

global A B C P z

F=(exp(A(1)-B(1)./(T+C(1))).*z(1))+(exp(A(2)-B(2)./(T+C(2))).*z(2))-P*760;