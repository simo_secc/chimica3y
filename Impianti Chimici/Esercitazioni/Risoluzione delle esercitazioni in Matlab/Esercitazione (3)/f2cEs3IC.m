function F=f2cEs3IC(T)

global A B C P xB

F=(exp(A(1)-B(1)./(T+C(1))).*xB(1))+(exp(A(2)-B(2)./(T+C(2))).*xB(2))-P*760;