function F=f2bEs3IC(T)

global A B C P xD

F=(P*760*xD(1)/exp(A(1)-B(1)/(T+C(1)))+P*760*xD(2)/exp(A(2)-B(2)/(T+C(2))))-1;