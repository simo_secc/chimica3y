function F=faEs6aIC(T)

global z AntA AntB AntC P

F=sum(((10.^(AntA-(AntB./(T+AntC-273.15))))./P).*z)-1;