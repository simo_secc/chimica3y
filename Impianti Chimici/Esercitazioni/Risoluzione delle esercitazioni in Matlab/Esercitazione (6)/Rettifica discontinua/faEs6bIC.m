function F=faEs6bIC(xD)

global alfa R z

y4=xD;
x4=y4/(alfa-y4*(alfa-1));
y3=(R/(R+1))*x4+xD/(R+1);
x3=y3/(alfa-y3*(alfa-1));
y2=(R/(R+1))*x3+xD/(R+1);
x2=y2/(alfa-y2*(alfa-1));
y1=(R/(R+1))*x2+xD/(R+1);
F=(y1/(alfa-y1*(alfa-1)))-z(1);

