%% Risoluzione dell'esercitazione (7) di Impianti Chimici: Calcolo di una unit� di assorbimento a stadi (Mattia Usuelli)
clc
clear all
close all
disp('------ Calcolo di una unit� di assorbimento a stadi ------')

% Dati: Per quanto riguarda la Y, si riferiscono al sistema benzene-aria,
% mentre per quanto riguarda la X si riferiscono al sistema benzene-olio:
y0=[0.05 0.95];
wNpiu1=[0.002 1-0.002];
T=300; % [K]
P=760; % [mmHg]
Q=1500; % [Nm^3/h]
assorb=0.9;
% I parametri delle Antoine si riferiscono a [benzene olio], nella forma
% logPev=A-B/(T+C), con T in [K] e Pev in [mmHg]
AntA=[15.9008 16.1510];
AntB=[2788.51 4294.55];
AntC=[-52.36 -124.0];
PM=[78.114 240.475]; % massa molecolare di benzene ed olio, misurata in [kg/kmol]

% Risoluzione dell'esercitazione:
% Come prima cosa, calcoliamo le tensioni di vapore di olio e benzene alla
% temperatura della colonna di assorbimento:
Pev=exp(AntA-AntB./(AntC+T)); % Possiamo notare che la tensione di vapore dell'olio � molto bassa, ed infatti nel testo � riportato che � molto poco volatile
alfa=Pev(1)/Pev(2); % determinazione della volatilit� relativa
Y0b=y0(1)/(1-y0(1)); 
disp('Dal momento che frazioni e rapporti molari sono significativamente diversi, trattiamo il problema come cascata non lineare')
% Considerando la corrente olio/benzene in condizioni di entrata, possiamo
% calcolare come segue il peso molecolare della miscela:
PMmix=(sum(wNpiu1./PM))^(-1); % [kg/kmol]
xNpiu1=wNpiu1*PMmix./PM;
XNpiu1=xNpiu1./(1-xNpiu1);
% In condizioni normali (273.15 [K] e 1 [atm]) una kmol di gas perfetto
% occupa un volume pari a 22.414 [m^3]. Di conseguenza possiamo scrivere
% che:
G0tot=Q/22.414; % [kmol/h]
G=G0tot*y0(2);
benzin=G0tot*y0(1);
benzr=assorb*benzin;
benzout=benzin-benzr;
YNb=benzout/G;
yNb=YNb/(1+YNb);
% Punto 1): Determinazione della quantit� di olio circolante nel sistema
disp('   ')
disp('Punto 1): Determinazione della quantit� di olio circolante nel sistema')
K=Pev(1)/P;
aprimoq=((1/(K-1))-XNpiu1(1))^2;
aprimo=sqrt(aprimoq);
B=K/(K-1);
bprimo=-4*XNpiu1(1)*B+2*aprimo*B-2*aprimo*YNb;
cprimo=YNb^2+B^2+2*B*YNb;
LGmin=[(-bprimo-sqrt(bprimo^2-4*aprimoq*cprimo))/(2*aprimoq) (-bprimo+sqrt(bprimo^2-4*aprimoq*cprimo))/(2*aprimoq)];
A=LGmin.*(1/(K-1))+(YNb-LGmin.*XNpiu1(1));
Ytg=(A-B)/2;
% Dei due valori ottenuti di Ytg, � accettabile solamente quello compreso
% tra Y0b e YNb, e di conseguenza nel nostro caso � accettabile solo il
% primo. Nel caso nessuno dei due valori limite fosse stato accettabile,
% avremmo dovuto determinare LGmin dalle condizioni non di tangenza ma di
% intersezione con la curva (in Y0b).
LGmin=LGmin(1);
LGeff=1.5*LGmin; % Il valore 1.5 � fornito dal testo
L=LGeff*G;
LNpiu1=L/(1-xNpiu1(1));
disp(['Il valore assunto dalla quantit� di olio circolante nel sistema � pari a ',num2str(L),' [kmol/h]'])

% Punto 2): Determinazione del numero di stadi teorici
disp('   ')
disp('Punto 2): Determinazione del numero di stadi teorici')
% Calcoliamo i valori di A, B e D nel caso di LGeff
A=LGeff*(1/(K-1))+(YNb-LGeff*XNpiu1(1));
B=K/(K-1);
D=(K/(K-1))*(YNb-LGeff*XNpiu1(1));
delta=(A-B)^2+4*D;
if delta>0
    Yi=(A-B+sqrt(delta))/2;
    N=log(((1/(YNb-Yi))-(1/(A-B-2*Yi)))/((1/(Y0b-Yi))-(1/(A-B-2*Yi))))/log((A-Yi)/(B+Yi));
else
    k=[-3 -2 -1 0 1 2 3];
    teta1=atan(sqrt(-delta)/(2*YNb-A+B));
    teta2=atan(sqrt(-delta)/(2*Y0b-A+B));
    teta3=atan(sqrt(-delta)/(A+B));
    N=(teta2-teta1+k.*pi)./teta3;
end
% Dopo aver fatto girare il programma, consideriamo il pi� piccolo valore
% positivo di N:
N=N(4);
disp(['Il numero di stadi teorici � pari a ',num2str(ceil(N))])
i=1;
Yn(1)=Y0b;
for i=1:ceil(N)
    Ynpiu1(i)=(D-B*Yn(i))/(Yn(i)-A);
    Yn(i+1)=Ynpiu1(i);
end
disp('Si guardi il grafico che mostra come varia Yn al variare del numero di stadi')
n=0:6;
plot(n,Yn)
xlabel('Stadio n')
ylabel('Rapporto molare di benzene in G')