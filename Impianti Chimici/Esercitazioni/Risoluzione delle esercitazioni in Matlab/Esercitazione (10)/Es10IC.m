%% Risoluzione dell'esercitazione (10) di Impianti Chimici: Progetto fluidodinamico di un piatto di una colonna benzene-toluene e calcolo dell'efficienza (Mattia Usuelli)

clc
clear all
close all

global Ql W 

% Dati:
P=1; % [atm]
T=273.15+93; % [K]
G=0.05; % [kmol/s]
L=0.040; % [kmol/s]
MMR=[85.8 85.8]; % [kg/kmol]
ro=[800 2.6]; % [kg/m^3]
sigma=[0.02 0]; % [N/m]
mu=[0 8.9e-6]; % [Pa*s]
Dabg=2.66e-6; % [m^2/s]
Dabl=2.2e-8; % [m^2/s]
g=9.81; % [m/s^2]

% Coefficienti della Antoine per il benzene, nella forma logPev=A-B/(T+C),
% con T in [K] e Pev in [mmHg]
AntA=15.9008;
AntB=2788.51;
AntC=-52.36;
d0=8e-3; % [m]
p=20e-3; % [m]
sp=2e-3; % [m]
t=60e-2; % [m]
vvf=0.8;
WD=0.7;
hw=5e-2; % [m]
hg=2.5e-2; % [m]
indisp=0.05;

% Risoluzione dell'esercitazione:
alfa=0.0744*t+0.01173;
beta=0.0304*t+0.015;
Lprimo=L*MMR(1);
Gprimo=G*MMR(2);
LGrogrol=(Lprimo/Gprimo)*sqrt(ro(2)/ro(1));
if LGrogrol < 0.1
    LGrogrol=0.1;
else
end
A0Aa=(pi/(2*sqrt(3)))*(d0/p)^2;
if A0Aa<0.1
    FA0Aa=5*(A0Aa)+0.5;
else
    FA0Aa=1;
end
Cf=FA0Aa*(alfa*log10(1/LGrogrol)+beta)*(sigma(1)/0.02)^0.2;
vfn=Cf*sqrt((ro(1)-ro(2))/ro(2));
v=vvf*vfn;
AdAtot=(asin(WD)-WD*sqrt(1-WD^2))/pi;
An=Gprimo/(ro(2)*v);
Atot=An/(1-AdAtot);
Ad=Atot*AdAtot;
D=sqrt(4*Atot/pi);
% Il valore di D risulta essere pari a 1.3543 [m], e risulta essere
% verificata l'hp che ci permette di considerare t pari a 0.6 [m].
% Arrotondiamo ora D al decimale superiore:
D=1.4;
Atot=pi*D^2/4;
Ad=AdAtot*Atot;
Ac=Atot-2*Ad;
Aa=(1-indisp)*Ac;
A0=Aa*A0Aa;
Ql=Lprimo/ro(1);
QlD=Ql/D;
W=WD*D;
X0=[1 1];
X=fsolve('fEs10IC',X0);
clc
disp('Risoluzione della esercitazione (10) di Impianti Chimici')
disp(['Il valore assunto dal diametro della colonna � pari a ',num2str(D)])
if QlD>3e-3 && QlD<1.5e-2
    disp('La condizione di flusso incrociato � verificata')
else
    disp('La condizione di flusso incrociato non � verificata')
end
WWeff=X(1);
h1=X(2);
disp(['Il valore assunto dalla altezza della cresta di liquido � pari a ',num2str(h1),' [m]'])
Qg=Gprimo/ro(2);
v0=Qg/A0;
Re0=d0*ro(2)*v0/mu(2);
C0=1.09*(d0/sp)^(1/4);
f=0.0791*Re0^(-1/4);
hD=0.5*ro(2)*v0^2*(1/(ro(1)*g))*C0*(0.40*(1.25-A0/An)+4*sp/d0*f+(1-A0/An)^2);
z=(W+D)/2;
va=Qg/Aa;
hL=0.0061+0.725*hw-0.238*hw*va*(ro(2))^0.5+1.225*Ql/z;
hR=6*sigma(1)/d0/(ro(1)*g);
hG=hD+hL+hR;
Ada=min(Ad,W*hg);
h2=3/(2*g)*(Ql/Ada)^2;
h3=hG+h2;
DP=hG*ro(1)*g;
disp(['Il valore assunto dalle perdite di carico per piatto � pari a ',num2str(DP),' [Pa]'])
if DP<800
    disp('Il valore delle perdite di carico, in quanto minore di 800 [Pa], � accettabile')
else
    disp('Il valore delle perdite di carico, in quanto maggiore di 800 [Pa], non � accettabile')
end
ingorgo=h1+hw+h3;
if ingorgo<t/2
    disp('Il valore assunto dalla grandezza "ingorgo" � accettabile, in quanto minore di t/2')
else
    disp('Il valore assunto dalla grandezza "ingorgo" non � accettabile, in quanto maggiore di t/2')
end
% Controllo su trascinamento e su gocciolamento:
Z=(Atot-2*Ad)/z;
lambda=2.8/((Z/d0)^0.724);
v0lim=sigma(1)/mu(2)*0.0229*(mu(2)^2/(sigma(1)*ro(2)*d0)*ro(1)/ro(2))^0.379*(sp/d0)^0.293*(2*Aa*d0/(sqrt(3)*p^3))^lambda;
if v0>v0lim
    disp('Poich� v0 � maggiore di v0lim, il controllo sul gocciolamento � verificato')
else
    disp('Poich� v0 � minore di v0lim, il controllo sul gocciolamento non � verificato')
end
% Per quanto riguarda il trascinamento, consideriamo il grafico in allegato
% con l'esercitazione. Calcoliamo il valore di LGrogrol (gi� determinato in
% precedenza) e consideriamo il valore assunto da v/vf (riportato sulle
% curve parametriche). Possiamo ora leggere sull'asse delle ordinate il
% valore assunto da E. Tale valore rappresenta le moli di L trascinate fratto la medesima grandezza sommata alla portata di L.
% Nel nostro caso:
E=0.072;
disp('Poich� il valore di E � inferiore a 0,1, il controllo sul trascinamento � verificato')
% Effettuiamo ora delle considerazioni sull'efficienza del piatto:
Scg=mu(2)/(ro(2)*Dabg);
tauL=hL*z*Z/Ql;
NUTg=(0.776+4.57*hw-0.238*va*ro(2)^0.5+104.6*Ql/Z)*Scg^(-0.5);
NUTl=4e4*(Dabl^0.5)*(0.213*va*ro(2)^0.5+0.15)*tauL;
k=exp(AntA-AntB/(T+AntC))/(P*760);
NUTi=(1/NUTg+G/L*k/NUTl)^(-1);
etaprimoV=NUTi/(1+NUTi);
De=(3.93e-3+0.0171*va+3.67*Ql/Z+0.18*hw)^2;
Pe=Z^2/(De*tauL);
alfa2=Pe/2*(sqrt(1+4*k*G*etaprimoV/(L*Pe))-1);
etaMV=etaprimoV*((1-exp(-alfa2-Pe))/((alfa2+Pe)*(1+(alfa2+Pe)/alfa))+(exp(alfa2)-1)/(alfa2*(1+alfa2/(alfa2+Pe))));
etaMVE=etaMV/(1+etaMV*(E/(1-E)));
disp(['Il valore assunto da etaprimoV � pari a ',num2str(etaprimoV)])
disp(['Il valore assunto da etaMV � pari a ',num2str(etaMV)])
disp(['Il valore assunto da etaMVE � pari a ',num2str(etaMVE)])
