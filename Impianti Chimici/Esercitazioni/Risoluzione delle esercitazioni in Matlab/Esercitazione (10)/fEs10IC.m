function F=fEs10IC(X)

global Ql W 

D=1.4;

WWeff=X(1);
h1=X(2);
F(1)=h1-0.666*(Ql/W)^(2/3)*WWeff^(2/3);
F(2)=WWeff^(-2)-(D/W)^2+(((D/W)^2-1)^0.5+2*h1/D*(D/W))^2;