%% Risoluzione dell'esercitazione (8) di Impianti Chimici: Assorbimento multicomponente (Mattia Usuelli)

clc
clear all
close all
format short
% Dati: Si riferiscono al sistema [metano etano propano n-butano olio]
y0=[0.70 0.15 0.10 0.05 0];
xNpiu1=[0 0 0 0.01 0.99];
AntA=[0 15.6637 15.7260 15.6782 0];
AntB=[0 1511.42 1872.46 2154.90 0];
AntC=[0 -17.16 -25.16 -34.42 0];
cpl=[0 105.12 116.35 138.60 377.00]; % [J/mol K]
cpv=[35.59 53.22 76.04 102.4 0]; % [J/mol K]
DHev=[0 10.03 16.58 22.53 0]*1e3; % [J/mol], riferito alla temperatura di 298 [K]
% I Coefficienti della Antoine si riferiscono alla forma logPev=A-B/(T+C),
% con T in [K] e Pev in [mmHg], inoltre i calori specifici sono medi
% rispetto alle temperatura di 273 e 313 [K]
Tin=298; % [K]
P=2*760; % [mmHg]
LNpiu1G0=3.5; % [kmol liq/kmol gas]
assorb=0.70;

%% Risoluzione delll'esercitazione secondo lo schema lineare:
disp('---------- Risoluzione della esercitazione secondo lo schema lineare ----------')
disp('1) Determinazione del numero di stadi necessari')
% Come prima cosa, possiamo definire nel seguente modo dei rapporti molari
% fittizi: Xin=Ln*xin/LN+1 e Yin=Gn*yin/G0. Utilizzando queste definizioni
% applicate allo stadio 0 per la corrente gassosa, e alla corrente N+1 per
% la corrente liquida, otteniamo che:
Y0=y0;
XNpiu1=xNpiu1;

% Come seconda cosa, scriviamo i bilanci molari relativi alle diverse specie,
% in modo tale da poter calcolare i rapporti molari allo stadio N e allo stadio 1:

% Componenti pi� leggeri del composto chiave (ossia il propano): si
% trasferiscono tutti in fase gassosa (simile a quanto avevamo assunto per
% le colonne di distillazione)
% LN+1*XiN+1+G0*Yi0=G0*Yin; LNpiu1G0*XiN+1+Yi0=YiN
for i=1:2
    YN(i)=Y0(i)+LNpiu1G0*XNpiu1(i);
end
for i=4:5
    YN(i)=0;
end
% Componenti pi� pesanti del composto chiave: si trasferiscono tutti in
% fase liquida (simile a quanto avevamo assunto per le colonne di
% distillazione)
% LN+1*XiN+1+G0*Yi0=LNpiu1*Xi1; LNpiu1G0*XiN+1+Yi0=LNpiu1G0Xi1 ; Xi1=XiN+1+Yi0/LNpiu1G0
for i=4:5
    X1(i)=XNpiu1(i)+Y0(i)/LNpiu1G0;
end
for i=1:2
    X1(i)=0;
end
% Composto chiave: per quanto riguarda tale componente, consideriamo il suo
% recupero: assorb*G0*Yk0=LN+1*Xk1;assorb*Yk0=LNpiu1G0*Xk1
X1(3)=assorb/LNpiu1G0*Y0(3);
% Considerando il bilancio molare completo per il composto chiave, possiamo
% calcolare il valore assunto da YN(3):
% LNpiu1G0*XkNpiu1+Yk0=YkN+LNpiu1G0*Xk1;
YN(3)=+LNpiu1G0*XNpiu1(3)+Y0(3)-LNpiu1G0*X1(3);
% Ora che abbiamo trovato i rapporti molari relativi alle correnti di
% uscita di tutti i componenti, � nostro interesse stimare il valore
% assunto da LG medio lungo la colonna di assorbimento, in modo tale da
% poter determinare il fettore di assorbimento A (che approssimeremo poi
% come costante lungo la colonna):

% In testa (n=N) abbiamo che:
LNpiu1GN=LNpiu1G0/sum(YN);
% In coda (n=1) abbiamo che:
L1G0=LNpiu1G0*sum(X1);
% Dunque il valor medio assunto da LG � pari a:
LGmedio=sqrt(LNpiu1GN*L1G0);
% Prima di poter calcolare i coefficienti A e/o S, � importante calcolare i
% valori assunti dalle k di equilibrio fisico dei vari componenti. Per
% quanto riguarda il metano, non sono riportati i coefficienti della
% Antoine, in quanto assumiamo che la sua k di equilibrio tenda ad infinito
% (il metano sta in fase gas e non ha la minima tendenza a spostarsi in
% fase liquida). Lo stesso discorso (ma opposto) vale per l'olio: la sua
% tensione di vapore � talmente bassa che tende a stare in fase liquida.
% Nel caso dell'olio verr� messo un valore della tensione di vapore tendente a 0,
% mentre nel caso del metano un valore di tensione di vapore molto alto (in
% modo tale che approssimi sufficientemente bene un valore infinito).
Pev(1)=1e10*760;
Pev(2)=exp(AntA(2)-AntB(2)/(Tin+AntC(2)));
Pev(3)=exp(AntA(3)-AntB(3)/(Tin+AntC(3)));
Pev(4)=exp(AntA(4)-AntB(4)/(Tin+AntC(4)));
Pev(5)=1e-10;
k=Pev./P;
Ai=LGmedio./k;
Si=1./Ai;
YNpiu1=Si.*XNpiu1.*LNpiu1G0;
YNpiu1(1)=0.7;
X0=Ai.*Y0./LNpiu1G0;
X0(5)=0.99;
Y1=X1.*LNpiu1G0;
Rk=(Y0(3)-YN(3))/(Y0(3)-YNpiu1(3));
N=log((Ai(3)-Rk)/(1-Rk))/log(Ai(3))-1;
disp(['Il numero di stadi necessari � pari a ',num2str(ceil(N))])

disp('   ')
disp('2) Stima delle portate e della composizione delle correnti uscenti')
L1LNpiu1=sum(X1);
GnG0=sum(YN);
yN=YN./GnG0;
x1=X1./L1LNpiu1;
disp(['Il rapporto fra L(1) e L(N+1) � pari a ',num2str(L1LNpiu1)])
disp(['Il rapporto fra G(N) e G(0) � pari a ',num2str(GnG0)])
disp('Si consideri il sistema S=[metano etano propano n-butano olio]')
disp(['La composizione della portata liquida uscente � ',num2str(x1)])
disp(['La composizione della portata gas uscente � ',num2str(yN)])
N=ceil(N);
for i=1:5
    for n=1:6
        % Nel caso di Y da uno a sei si intende dal pedice 0 fino al pedice
        % 5, mentre per quanto riguarda la X il valore di n corrisponde a
        % quello del pedice
        Y(n,i)=YNpiu1(i)*((Ai(i)^(N+1)-Ai(i)^(N+1-(n-1)))/(Ai(i)^(N+1)-1))+Y0(i)*((Ai(i)^(N+1-(n-1))-1)/(Ai(i)^(N+1)-1));
        X(n,i)=XNpiu1(i)*((1-Si(i)^(n-1))/(1-Si(i)^(N+1)))+X0(i)*((Si(i)^(n-1)-Si(i)^(N+1))/(1-Si(i)^(N+1)));
    end
end


disp('   ')
disp('Verifica della cascata lineare')
disp('Le righe delle matrici rappresentano le diverse sostanze, secondo il vettore S=[metano etano propano n-butano olio]')
disp('Le colonne delle matrici rappresentano i diversi stadi, dallo 0 fino ad N+1')
disp('I rapporti molari delle sostanze in fase liquida nei diversi stadi sono: ')
disp(X)
disp('I rapporti molari delle sostanze in fase gas nei diversi stadi sono: ')
disp(Y)

sumX0=sum(X(1,1:5));
sumX1=sum(X(2,1:5));
sumX2=sum(X(3,1:5));
sumX3=sum(X(4,1:5));
sumX4=sum(X(5,1:5));
sumX5=sum(X(6,1:5));

sumY0=sum(Y(1,1:5));
sumY1=sum(Y(2,1:5));
sumY2=sum(Y(3,1:5));
sumY3=sum(Y(4,1:5));
sumY4=sum(Y(5,1:5));
sumY5=sum(Y(6,1:5));

for i=1:5
        y(1,i)=Y(1,i)/sumY0;
        y(2,i)=Y(2,i)/sumY1;
        y(3,i)=Y(3,i)/sumY2;
        y(4,i)=Y(4,i)/sumY3;
        y(5,i)=Y(5,i)/sumY4;
        y(6,i)=Y(6,i)/sumY5;
        x(1,i)=X(1,i)/sumX0;
        x(2,i)=X(2,i)/sumX1;
        x(3,i)=X(3,i)/sumX2;
        x(4,i)=X(4,i)/sumX3;
        x(5,i)=X(5,i)/sumX4;
        x(6,i)=X(6,i)/sumX5;
end        

disp('Le frazioni molari delle sostanze in fase liquida nei diversi stadi sono: ')
disp(x)
disp('Le frazioni molari delle sostanze in fase gas nei diversi stadi sono: ')
disp(y)


%% Risoluzione dell'esercitazione secondo lo schema di Edminster:
% Ipotizziamo che la portata entrante di gas sia pari ad 1 [kmol/h]
disp('   ')
disp('---------- Risoluzione della esercitazione secondo lo schema di Edminster ----------')
G0=1; % [kmol/h]
GN=G0*GnG0;
LNpiu1=LNpiu1G0*G0;
L1=L1LNpiu1*LNpiu1;
G0GNN=(G0-GN)/4;
G1=G0-G0GNN;
LN=LNpiu1+G0GNN;
% Rimuoviamo ora l'ipotesi di temperatura uniforme nei diversi stadi:
Lcplmedio=LNpiu1*sqrt((sum(XNpiu1.*cpl))*(sum(X1.*cpl)));
Gcpvmedio=G0*sqrt((sum(YN.*cpv))*(sum(Y0.*cpv)));
R=Gcpvmedio/Lcplmedio;
DHevast=(Y0-YN).*DHev*G0;
DHevasttot=sum(DHevast);
DTast=DHevasttot/(N*Lcplmedio);
for n=1:6
    DT(n)=DTast/(1-R)*((N+1)*(1-R^(n-1))/(1-R^(N+1))-(n-1));
end
DT1=DT(2);
DTN=DT(5);
T1=Tin+DT1;
TN=Tin+DTN;
% Ora che abbiamo calcolato le temperature agli stadi 1 e 4, possiamo
% calcolare le k di equilibrio fisico e i fattori di assorbimento:
k1=exp(AntA-AntB./(T1+AntC))/P;
k1(1)=1e20;
k1(5)=1e-20;
kN=exp(AntA-AntB./(TN+AntC))/P;
kN(1)=1e20;
kN(5)=1e-20;
A1=(L1/G1)./k1;
AN=(LN/GN)./kN;
AE=sqrt(A1.*(1+AN)+(1/4))-(1/2);
SE=1./AE;
% Possiamo ora procedere come in precedenza, a verificare le diverse
% frazioni molari e rapporti molari nei diversi stadi:

for i=1:5
    for n=1:6
        % Nel caso di Y da uno a sei si intende dal pedice 0 fino al pedice
        % 5, mentre per quanto riguarda la X il valore di n corrisponde a
        % quello del pedice
        Y(n,i)=YNpiu1(i)*((AE(i)^(N+1)-AE(i)^(N+1-(n-1)))/(AE(i)^(N+1)-1))+Y0(i)*((AE(i)^(N+1-(n-1))-1)/(AE(i)^(N+1)-1));
        X(n,i)=AE(i)*Y(n,i)/LNpiu1G0;
    end
end

for i=5
    for n=1:6
        X(n,i)=0.99;
    end
end

disp('   ')
disp('Verifica della cascata lineare')
disp('Le righe delle matrici rappresentano le diverse sostanze, secondo il vettore S=[metano etano propano n-butano olio]')
disp('Le colonne delle matrici rappresentano i diversi stadi, dallo 0 fino ad N+1')
disp('I rapporti molari delle sostanze in fase liquida nei diversi stadi sono: ')
disp(X)
disp('I rapporti molari delle sostanze in fase gas nei diversi stadi sono: ')
disp(Y)

sumX0=sum(X(1,1:5));
sumX1=sum(X(2,1:5));
sumX2=sum(X(3,1:5));
sumX3=sum(X(4,1:5));
sumX4=sum(X(5,1:5));
sumX5=sum(X(6,1:5));

sumY0=sum(Y(1,1:5));
sumY1=sum(Y(2,1:5));
sumY2=sum(Y(3,1:5));
sumY3=sum(Y(4,1:5));
sumY4=sum(Y(5,1:5));
sumY5=sum(Y(6,1:5));

for i=1:5
        y(1,i)=Y(1,i)/sumY0;
        y(2,i)=Y(2,i)/sumY1;
        y(3,i)=Y(3,i)/sumY2;
        y(4,i)=Y(4,i)/sumY3;
        y(5,i)=Y(5,i)/sumY4;
        y(6,i)=Y(6,i)/sumY5;
        x(1,i)=X(1,i)/sumX0;
        x(2,i)=X(2,i)/sumX1;
        x(3,i)=X(3,i)/sumX2;
        x(4,i)=X(4,i)/sumX3;
        x(5,i)=X(5,i)/sumX4;
        x(6,i)=X(6,i)/sumX5;
end        

disp('Le frazioni molari delle sostanze in fase liquida nei diversi stadi sono: ')
disp(x)
disp('Le frazioni molari delle sostanze in fase gas nei diversi stadi sono: ')
disp(y)
