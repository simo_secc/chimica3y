function F=fbEs14IC(fc)

global Rec2

F=1/sqrt(fc)-4*log10(Rec2*sqrt(fc))+0.4;