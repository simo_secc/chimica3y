%% Risoluzione dell'esercitazione (13) di Impianti Chimici: Progetto di uno scambiatore a fascio tubiero (Mattia Usuelli)
clc
clear all
close all
% Dati:
% I dati espressi in seguito si riferiscono al sistema S=[A B]; t si
% riferisce al fluido che si scalda (A), mentre T a quello che si raffredda
% (B)
WA=12.5; % [kg/s]
tin=92+273.15; % [K]
tout=115+273.15; % [K]
Tin=162+273.15; % [K]
Tout=120+273.15; % [K]
Rs=2e-4; % [(m^2 K)/W]
OD=2.54*(3/4)*1e-2; % [m]
L=5; % [m]
p=2.54*1e-2; % [m]
U0=442; % [W/(m^2 K)]
cp=[2512 2261]; % [J/(kg K)]
ro=[710 770]; % [kg/m^3]
k=[0.146 0.127]; % [W/(m K)]
B=0.1; % [m], corrisponde alla distanza fra i diaframmi
% Risoluzione:
% Come prima cosa, determiniamo la portata massiva di B, in modo tale da
% poter determinare quale dei due fluidi � conveniente porre nel lato tubo
% e quale nel lato mantello:
disp('Progetto di uno scambiatore a fascio tubiero')
Q=WA*cp(1)*(tout-tin);
WB=Q/(cp(2)*(Tin-Tout));
disp('Poich� la portata massiva di B � minore di quella di A, poniamo A nel lato tubo, e B nel lato mantello')
DTml=((Tin-tout)-(Tout-tin))/log((Tin-tout)/(Tout-tin));
% Per calcolare il DTeff, � necessario prima calcolare il fattore di
% correzione F:
E=(tin-tout)/(tin-Tin);
R=(Tin-Tout)/(tout-tin);
F=sqrt(1+R^2)/(R-1)*log((1-E)/(1-E*R))/log((2-E*(R+1-sqrt(R^2+1)))/(2-E*(R+1+sqrt(R^2+1))));
DTeff=DTml*F;
% Il valore di kc lo prendiamo dal diagramma a pagina (3)
% dell'esercitazione: nel nostro caso abbiamo che DTA=42[K] e DTB=23[K],
% dai cui derivano dei valori di kc rispettivamente di 0.2 e 0.1 (circa).
% Dal momento che dobbiamo considerare il massimo di questi due valori,
% abbiamo che:
kc=0.2;
% Con DTfreddo e DTcaldo si intendono le differenze di temperatura dei due
% fluidi al lato caldo ed al lato freddo dello scambiatore:
DTcaldo=Tin-tout;
DTfreddo=Tout-tin;
r=DTfreddo/DTcaldo;
Fc=((1/kc)+(r/(r-1)))/(1+log(kc+1)/log(r))-1/kc;
tAcal=tin+Fc*(tout-tin);
TBcal=Tout+Fc*(Tin-Tout);
% Per quanto riguarda la determinazione della viscosit� dei due fluidi,
% consideriamo la T calorica espressa in gradi e, con l'aiuto le
% coordinate x/y fornite dei dati del testo, usiamo il grafico a pagina
% (4). Nel nostro caso otteniamo che:
mu=[0.6 0.66]*1e-3; % [Pa*s]
% Possiamo ora calcolare la A di primo tentativo:
A=Q/(U0*DTeff); % [m^2]
Atubo=pi*OD*L; % [m^2]
NT=A/Atubo; % numero di tubi
NT=ceil(NT);
disp(['Il numero di tubi teorico � pari a ',num2str(NT)])
% Consideriamo ora la tabella a pagina (6) dell'esercitazione: considerando
% un numero di passaggi lato tubi pari a quello desiderato (ossia due) e
% considerando il caso con passo uguale ad un pollice, possiamo notare che
% i tubi possono essere 166 o 220 (numeri pi� vicini a 173). Scegliamo il
% numero superiore, ottenendo dunque:
NT=220;
disp(['Il numero di tubi reale � pari a ',num2str(NT)])
IDshell=19.25*2.54/100; % [m]
Aeffettiva=NT*Atubo;
Ugeom=Q/(Aeffettiva*DTeff); % [W/(m^2 K)]
disp(['Il valore assunto da Ugeom � pari a ',num2str(Ugeom),' [W/(m^2 K)]'])
% Determiniamo ora l'U fluidodinamico:

% Lato mantello (shell):
c=p-OD;
a=B*IDshell*c/p;
G=WB/a;
Apassaggio=p^2-pi*OD^2/4;
pbagnato=pi*OD;
Deq=4*Apassaggio/pbagnato;
ReDeq=G*Deq/mu(2);
Pr=mu(2)*cp(2)/k(2);
Nu=0.156*ReDeq^0.6*Pr^(1/3);
hshell=Nu*k(2)/Deq; % [W/(m^2 K)]
% Lato tubi: Dalla tabella a pagina (5), possiamo leggere che nel caso di
% tubi 16 BWG, lo spessore � pari a:
s=0.065*2.54*1e-2; % [m]
ID=OD-2*s;
G=WA/(NT*pi*ID^2/4*0.5);
Re=G*ID/mu(1);
Pr=mu(1)*cp(1)/k(1);
Nu=0.023*Re^(4/5)*Pr^(1/3);
htubi=Nu*k(1)/ID;
Ufluid=(1/htubi+1/hshell+2*Rs)^(-1); % [W/(m^2 K)], ammettiamo che Rs sia la medesima da entrambi i lati del tubo
disp(['Il valore assunto da Ufluid � pari a ',num2str(Ufluid),' [W/(m^2 K)]'])
disp('Poich� Ufluid � maggiore di Ugeom, lo scambiatore pu� funzionare');
%  Valutiamo ora le perdite di carico:

% Lato tubi:
v=G/ro(1);
f=0.048*Re^(-0.2);
fast=f*1.2;
NPT=2;
DPtott=(ro(1)*v^2/2*4*fast*L/ID+2.5*ro(1)*v^2/2)*NPT; % [Pa]
disp(['Le perdite di carico nei tubi sono pari a ',num2str(DPtott),' [Pa]'])
disp('Le perdite di carico nei tubi sono accettabili, in quanto minori di 70 [kPa]')

% Lato mantello:
G=WB/a;
v=G/ro(2);
NB=L/B-1;
fs=1.686*ReDeq^(-0.1878);
DPtots=ro(2)*v^2/2*IDshell/Deq*(NB+1)*fs; % [Pa]
disp(['Le perdite di carico nel mantello sono pari a ',num2str(DPtots),' [Pa]'])
disp('Le perdite di carico nel mantello sono accettabili, in quanto minori di 70 [kPa]')
% Se le perdite di carico fossero state troppo elevate, avremmo potuto
% diminuire il numero di diaframmi.