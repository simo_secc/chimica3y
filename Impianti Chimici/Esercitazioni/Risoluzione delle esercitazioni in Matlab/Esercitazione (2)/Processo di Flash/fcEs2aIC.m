function F=fcEs2aIC(X)

global z A B C

P=4;
alfa=0.45;
k=(exp(A-B./(X+C)))/760/P;

F=sum(z.*(k-1)./(1+alfa.*(k-1)));