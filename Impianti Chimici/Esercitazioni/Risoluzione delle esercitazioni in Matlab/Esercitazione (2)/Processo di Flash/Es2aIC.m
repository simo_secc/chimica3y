%% Risoluzione dell'esercitazione "Processo di flash" (Mattia Usuelli)

clc
clear all
close all
global z k A B C cpl DHev Tnb Pc Tc
disp('Risoluzione della esercitazione "Processo di flash"')
%% Dati: Si riferiscono al sistema S=[n-butano n-pentano];
z=[0.5 0.5];
F=100; %[mol/s]
% Parametri delle Antoine:
A=[15.6782 15.8333];
B=[2154.90 2477.07];
C=[-34.42 -39.94];
cpl=[133.601 173.334]; %[J/mol K]
DHev=[22400 25800]; %[J/mol]
Tnb=[272.7 309.2]; %[K]
Pc=[37.5 33.3]; %[atm]
Tc=[425.2 469.6]; %[K]
Trif=343.15; %[K]
%% Risoluzione dell'esercitazione:

% Caso a)
disp('------ Caso a) ------')
T=343.15; %[K]
P=5; %[atm]
k=(exp(A-B./(T+C)))/760/P;
X0=0.5;
alfa=fzero('faEs2aIC',X0);
x=z./(1+alfa.*(k-1));
y=k.*x;
hl=cpl.*(T-Trif);
TR=T./Tc;
TRnb=Tnb/Tc;
DHevT=DHev.*((1-TR)./(1-TRnb)).^0.38;
hv=hl+DHevT;
hltot=sum(hl.*x);
hvtot=sum(hv.*y);
Q=alfa*hvtot+(1-alfa)*hltot;
disp(['Il valore assunto da alfa � pari a ',num2str(alfa)])
disp(['Il valore assunto dalle frazioni molari in fase liquida � pari a ',num2str(x)])
disp(['Il valore assunto dalle frazioni molari in fase gas � pari a ',num2str(y)])
disp(['Il valore assunto dal duty � pari a ',num2str(Q),' [W]'])

% Caso b)
disp('------ Caso b) ------')
T=328.15; %[K]
RRV(1)=0.4566;
RRL(1)=1-RRV(1);
Pev=(exp(A-B./(T+C)))/760;
for i=1:2
    for j=1:2
        alfa(i,j)=Pev(i)/Pev(j);
    end
end
SF(1)=RRV(1)/RRL(1);
SF(2)=SF(1)*alfa(2,1);
RRV(2)=SF(2)/(1+SF(2));
RRL(2)=1-RRV(2);
Lx=RRL.*F.*z;
Vy=RRV.*F.*z;
V=sum(Vy);
L=sum(Lx);
x=Lx./L;
y=Vy./V;
P=sum(Pev.*x);
alfa=V/L;
hl=cpl.*(T-Trif);
TR=T./Tc;
TRnb=Tnb./Tc;
DHevT=DHev.*((1-TR)./(1-TRnb)).^0.38;
hv=hl+DHevT;
hltot=sum(hl.*x);
hvtot=sum(hv.*y);
Q=alfa*hvtot+(1-alfa)*hltot;
disp(['Il valore assunto da P � pari a ',num2str(P),' [atm]'])
disp(['Il valore assunto dalle frazioni molari in fase liquida � pari a ',num2str(x)])
disp(['Il valore assunto dalle frazioni molari in fase gas � pari a ',num2str(y)])
disp(['Il valore assunto dalla portata L � pari a ',num2str(L),' [mol/s]'])
disp(['Il valore assunto dalla portata V � pari a ',num2str(V),' [mol/s]'])
disp(['Il valore assunto dal duty � pari a ',num2str(Q),' [W]'])

% Caso c)
disp('------ Caso c) ------')
P=4; %[atm]
clear alfa
alfa=0.45;
X0=300;
T=fzero('fcEs2aIC',X0);
k=(exp(A-B./(T+C)))/760/P;
x=z./(1+alfa.*(k-1));
y=k.*x;
hl=cpl.*(T-Trif);
TR=T./Tc;
TRnb=Tnb./Tc;
DHevT=DHev.*((1-TR)./(1-TRnb)).^0.38;
hv=hl+DHevT;
hltot=sum(hl.*x);
hvtot=sum(hv.*y);
Q=alfa*hvtot+(1-alfa)*hltot;
disp(['Il valore assunto da T � pari a ',num2str(T),' [K]'])
disp(['Il valore assunto dalle frazioni molari in fase liquida � pari a ',num2str(x)])
disp(['Il valore assunto dalle frazioni molari in fase gas � pari a ',num2str(y)])
disp(['Il valore assunto dal duty � pari a ',num2str(Q),' [W]'])

% Caso d)
disp('------ Caso d) ------')
P=3.85; %[atm]
RRV(1)=0.6355;
X0=300;
T=fzero('fdEs2aIC',X0);
k=(exp(A-B./(T+C)))/760/P;
x=z./(1+alfa.*(k-1));
y=k.*x;
hl=cpl.*(T-Trif);
TR=T./Tc;
TRnb=Tnb./Tc;
DHevT=DHev.*((1-TR)./(1-TRnb)).^0.38;
hv=hl+DHevT;
hltot=sum(hl.*x);
hvtot=sum(hv.*y);
Q=alfa*hvtot+(1-alfa)*hltot;
disp(['Il valore assunto da T � pari a ',num2str(T),' [K]'])
disp(['Il valore assunto dalle frazioni molari in fase liquida � pari a ',num2str(x)])
disp(['Il valore assunto dalle frazioni molari in fase gas � pari a ',num2str(y)])
disp(['Il valore assunto dal duty � pari a ',num2str(Q),' [W]'])

% Caso e)
disp('------ Caso e) ------')
P=3; %[atm]
alfa(1)=0.5;
T0=320;
T(1)=fzero('fe1Es2aIC',T0);
k=(exp(A-B./(T(1)+C)))/760/P;
x=z./(1+alfa(1).*(k-1));
y=k.*x;
hl=cpl.*(T(1)-Trif);
TR=T(1)./Tc;
TRnb=Tnb./Tc;
DHevT=DHev.*((1-TR)./(1-TRnb)).^0.38;
hv=hl+DHevT;
hltot=sum(hl.*x);
hvtot=sum(hv.*y);
alfa(2)=-hltot/(hvtot-hltot);
T0=320;
T(2)=fzero('fe2Es2aIC',T0);
Trif=343.15;
k=(exp(A-B./(T(2)+C)))/760/P;
x=z./(1+alfa(2).*(k-1));
y=k.*x;
hl=cpl.*(T(2)-Trif);
TR=T(2)./Tc;
TRnb=Tnb./Tc;
DHevT=DHev.*((1-TR)./(1-TRnb)).^0.38;
hv=hl+DHevT;
hltot=sum(hl.*x);
hvtot=sum(hv.*y);
alfa(3)=-hltot/(hvtot-hltot);
T0=320;
T(3)=fzero('fe3Es2aIC',T0);
Trif=343.15;
k=(exp(A-B./(T(3)+C)))/760/P;
x=z./(1+alfa(3).*(k-1));
y=k.*x;
hl=cpl.*(T(3)-Trif);
TR=T(3)./Tc;
TRnb=Tnb./Tc;
DHevT=DHev.*((1-TR)./(1-TRnb)).^0.38;
hv=hl+DHevT;
hltot=sum(hl.*x);
hvtot=sum(hv.*y);
alfa(4)=-hltot/(hvtot-hltot);
T0=320;
T(4)=fzero('fe4Es2aIC',T0);
Trif=343.15;
k=(exp(A-B./(T(4)+C)))/760/P;
x=z./(1+alfa(4).*(k-1));
y=k.*x;
hl=cpl.*(T(4)-Trif);
TR=T(4)./Tc;
TRnb=Tnb./Tc;
DHevT=DHev.*((1-TR)./(1-TRnb)).^0.38;
hv=hl+DHevT;
hltot=sum(hl.*x);
hvtot=sum(hv.*y);
alfa(5)=-hltot/(hvtot-hltot);
T0=320;
T(5)=fzero('fe5Es2aIC',T0);
Trif=343.15;
k=(exp(A-B./(T(5)+C)))/760/P;
x=z./(1+alfa(5).*(k-1));
y=k.*x;
hl=cpl.*(T(5)-Trif);
TR=T(5)./Tc;
TRnb=Tnb./Tc;
DHevT=DHev.*((1-TR)./(1-TRnb)).^0.38;
hv=hl+DHevT;
hltot=sum(hl.*x);
hvtot=sum(hv.*y);
alfa(6)=-hltot/(hvtot-hltot);
k=(exp(A-B./(T(5)+C)))/760/P;
x=z./(1+alfa(6).*(k-1));
y=k.*x;
disp(['Il valore assunto da T � pari a ',num2str(T(5)),' [K]'])
disp(['Il valore assunto dalle frazioni molari in fase liquida � pari a ',num2str(x)])
disp(['Il valore assunto dalle frazioni molari in fase gas � pari a ',num2str(y)])
