function F=fe4Es2aIC(X)

global z A B C

P=3;
alfa=0.153589200364722;
k=(exp(A-B./(X+C)))/760/P;

F=sum(z.*(k-1)./(1+alfa.*(k-1)));