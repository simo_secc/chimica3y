function F=fdEs2aIC(X)

global z A B C

P=3.85; %[atm]
RRV(1)=0.6355;
k=(exp(A-B./(X+C)))/760/P;

F=sum(z.*(k-1)./((k-k(1))*RRV(1)+k(1)));