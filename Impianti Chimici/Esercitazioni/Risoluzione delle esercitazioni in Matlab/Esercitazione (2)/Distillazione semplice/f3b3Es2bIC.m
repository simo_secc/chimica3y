function F=f3b3Es2bIC(X)

global z k3

F=sum(z.*(k3-1)./(1+X.*(k3-1)));