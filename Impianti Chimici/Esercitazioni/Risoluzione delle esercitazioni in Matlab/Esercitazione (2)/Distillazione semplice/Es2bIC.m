%% Risoluzione dell'esercitazione "Distillazione semplice" (Mattia Usuelli)
clc
clear all
close all
disp('Risoluzione della esercitazione "Distillazione semplice"')
disp('I risultati rappresentati sotto forma vettoriale si riferiscono alle specie S=[propano i-butano n-butano n-pentano]')

global z k A B C P alfa k1 k2 k3

%% Dati: Si riferiscono al sistema S=[propano i-butano n-butano n-pentano];
Tin=293.15; %[K]
P=4; %[bar]
R=8.314; %[J/mol K]
alfa=0.6; %V/F = rapporto di vaporizzazione
z=[0.25 0.15 0.32 0.28];
PM=[44.097 58.124 58.124 72.151]*1e-3; %[kg/mol]
A=[15.7260 15.5381 15.6782 15.833];
B=[1872.46 2032.73 2154.90 2477.07];
C=[-25.16 -33.15 -34.42 -39.94];
cpl=[106.345 133.559 133.601 173.334]; %[J/mol K]
DHevnb=[18800 21300 22400 25800]; %[J/mol]
Tnb=[231.1 261.3 272.7 309.2]; %[K]
Pc=[42.01 36.00 37.47 33.31]; %[atm]
Tc=[369.90 408.10 425.20 469.60]; %[K]
om=[0.15240 0.18479 0.20100 0.25389]; % fattore acentrico di Pitzer
Pev=exp(A-B./(Tin+C))./760; % Tensione di vapore nelle condizioni di ingresso
Pb=sum(Pev.*z); % Pressione di bolla nelle condizioni di ingresso

%% Risoluzione dell'esercitazione

% Punto 1): determinazione della composizione massiva dell'alimentazione
disp('------ Punto 1) ------')
PMmix=sum(z.*PM);
omm=z.*PM/PMmix;
disp(['I valori assunti dalle frazioni massive in alimentazione sono ', num2str(omm)])

% Punto 2)
disp('------ Punto 2) ------')
F=1; %[mol/s] � una nostra scelta
X0=300;
T=fzero('f2Es2bIC',X0);
k=(exp(A-B./(T+C)))/760/P;
x=z./(1+alfa.*(k-1));
y=k.*x;
Trif=Tin; %[K]
hl=cpl.*(T-Trif);
TR=T./Tc;
TRnb=Tnb./Tc;
DHev=DHevnb.*((1-TR)./(1-TRnb)).^0.38;
hv=hl+DHev;
hltot=sum(hl.*x);
hvtot=sum(hv.*y);
Q=alfa*hvtot+(1-alfa)*hltot;
V=alfa*F;
RRV=(V.*y)./(F.*z);
RRL=1-RRV;
disp(['Il valore assunto da T � pari a ',num2str(T),' [K]'])
disp(['I valori assunti dalle frazioni molari in fase liquida sono pari a ',num2str(x)])
disp(['I valori assunti dalle frazioni molari in fase vapore sono pari a ',num2str(y)])
disp(['I valori assunti dai rapporti di recupero in fase liquida sono pari a ',num2str(RRL)])
disp(['I valori assunti dai rapporti di recupero in fase vapore sono pari a ',num2str(RRV)])
disp(['Il valore assunto dal duty � pari a ',num2str(Q),' [W]'])

% Punto 3)
disp('------ Punto 3) ------')
disp('Caso a): miscela ideale')
clear alfa,
X0=0.5;
alfa=fzero('f3aEs2bIC',X0);
x=z./(1+alfa.*(k-1));
y=k.*x;
hl=cpl.*(T-Trif);
TR=T./Tc;
TRnb=Tnb./Tc;
DHev=DHevnb.*((1-TR)./(1-TRnb)).^0.38;
hv=hl+DHev;
hltot=sum(hl.*x);
hvtot=sum(hv.*y);
Q=alfa*hvtot+(1-alfa)*hltot;
V=alfa*F;
RRV=(V.*y)./(F.*z);
RRL=1-RRV;
disp(['Il valore assunto da alfa � pari a ',num2str(alfa)])
disp(['Il valore assunto dalle frazioni molari in fase liquida � pari a ',num2str(x)])
disp(['Il valore assunto dalle frazioni molari in fase gas � pari a ',num2str(y)])
disp(['I valori assunti dai rapporti di recupero in fase liquida sono pari a ',num2str(RRL)])
disp(['I valori assunti dai rapporti di recupero in fase vapore sono pari a ',num2str(RRV)])
disp(['Il valore assunto dal duty � pari a ',num2str(Q),' [W]'])
disp('Caso b): miscela non ideale')
x0=x;
y0=y; % Valori di primo tentativo, dedotti dal caso di idealit�
m=0.47979+1.576.*om-0.1925.*(om.^2)+0.025.*(om.^3);
OmegaA=0.42748;
OmegaB=0.08664;
a=(OmegaA.*(R^2).*(Tc.^2)./Pc).*((1+m.*(1-sqrt(T./Tc))).^2);
b=OmegaB.*R.*Tc./Pc;
al=(sum(x0.*sqrt(a)))^2;
bl=sum(x0.*b);
av=(sum(y0.*sqrt(a)))^2;
bv=sum(y0.*b);
Al=al*P/(R*T)^2;
Bl=bl*P/(R*T);
Av=av*P/(R*T)^2;
Bv=bv*P/(R*T);
coeffl=[1 -1 (Al-Bl-Bl^2) -Al*Bl];
coeffv=[1 -1 (Av-Bv-Bv^2) -Av*Bv];
Zl=roots(coeffl);
Zl=sort(Zl);
Zl=Zl(1);
Zv=roots(coeffv);
Zv=sort(Zv);
Zv=Zv(3);
fil1=((1/(Zl-Bl))*(1+Bl/Zl).^(-(Al/Bl).*(2*sqrt(a./al)-b./bl))).*exp((b./bl).*(Zl-1));
fiv1=((1/(Zv-Bv))*(1+Bv/Zv).^(-(Av/Bv).*(2*sqrt(a./av)-b./bv))).*exp((b./bv).*(Zv-1));
k1=fil1./fiv1;
X0=0.5;
alfa1=fzero('f3b1Es2bIC',X0);
x1=z./(1+alfa1.*(k1-1));
y1=k1.*x;
al=(sum(x1.*sqrt(a)))^2;
bl=sum(x1.*b);
av=(sum(y1.*sqrt(a)))^2;
bv=sum(y1.*b);
Al=al*P/(R*T)^2;
Bl=bl*P/(R*T);
Av=av*P/(R*T)^2;
Bv=bv*P/(R*T);
coeffl=[1 -1 (Al-Bl-Bl^2) -Al*Bl];
coeffv=[1 -1 (Av-Bv-Bv^2) -Av*Bv];
Zl=roots(coeffl);
Zl=sort(Zl);
Zl=Zl(1);
Zv=roots(coeffv);
Zv=sort(Zv);
Zv=Zv(3);
fil2=((1/(Zl-Bl))*(1+Bl/Zl).^(-(Al/Bl).*(2*sqrt(a./al)-b./bl))).*exp((b./bl).*(Zl-1));
fiv2=((1/(Zv-Bv))*(1+Bv/Zv).^(-(Av/Bv).*(2*sqrt(a./av)-b./bv))).*exp((b./bv).*(Zv-1));
k2=fil2./fiv2;
X0=0.5;
alfa2=fzero('f3b2Es2bIC',X0);
x2=z./(1+alfa2.*(k2-1));
y2=k2.*x;
al=(sum(x2.*sqrt(a)))^2;
bl=sum(x2.*b);
av=(sum(y2.*sqrt(a)))^2;
bv=sum(y2.*b);
Al=al*P/(R*T)^2;
Bl=bl*P/(R*T);
Av=av*P/(R*T)^2;
Bv=bv*P/(R*T);
coeffl=[1 -1 (Al-Bl-Bl^2) -Al*Bl];
coeffv=[1 -1 (Av-Bv-Bv^2) -Av*Bv];
Zl=roots(coeffl);
Zl=sort(Zl);
Zl=Zl(1);
Zv=roots(coeffv);
Zv=sort(Zv);
Zv=Zv(3);
fil3=((1/(Zl-Bl))*(1+Bl/Zl).^(-(Al/Bl).*(2*sqrt(a./al)-b./bl))).*exp((b./bl).*(Zl-1));
fiv3=((1/(Zv-Bv))*(1+Bv/Zv).^(-(Av/Bv).*(2*sqrt(a./av)-b./bv))).*exp((b./bv).*(Zv-1));
k3=fil3./fiv3;
X0=0.5;
alfa3=fzero('f3b3Es2bIC',X0);
x3=z./(1+alfa3.*(k3-1));
y3=k3.*x3;
hl=cpl.*(T-Trif);
TR=T./Tc;
TRnb=Tnb./Tc;
DHev=DHevnb.*((1-TR)./(1-TRnb)).^0.38;
hv=hl+DHev;
hltot=sum(hl.*x3);
hvtot=sum(hv.*y3);
Q=alfa*hvtot+(1-alfa)*hltot;
V=alfa*F;
RRV=(V.*y3)./(F.*z);
RRL=1-RRV;
disp(['Il valore assunto da alfa � pari a ',num2str(alfa3)])
disp(['Il valore assunto dalle frazioni molari in fase liquida � pari a ',num2str(x3)])
disp(['Il valore assunto dalle frazioni molari in fase gas � pari a ',num2str(y3)])
disp(['I valori assunti dai rapporti di recupero in fase liquida sono pari a ',num2str(RRL)])
disp(['I valori assunti dai rapporti di recupero in fase vapore sono pari a ',num2str(RRV)])
disp(['Il valore assunto dal duty � pari a ',num2str(Q),' [W]'])